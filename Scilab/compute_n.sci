//*  compute_n.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Cherche jusqu'à quelle dérivées il est nécessaire d'aller pour
// voir apparaitre la commande de poursuite dans Y (la sortie).
//	Dans le cas d'un système multi-sorties il est judicieux de décomposer
// C afin d'avoir un n différent pour chaque sortie.
//
//	Pour i < n :
//		y^(i) = C As^n x
//
//	Et pour i = n :
//		y^(i) = C As^n x + C As^{n-1} B u_p
//
//*  Arguments :
//
//	As : Matrice de dynamique stable (n*n)
//	B : Matrice de commande (n*p)
//	C : Matrice d'observation (q*n)
//
//*  Retour :
//
//	n : ordre de la première dérivé laissant apparaitre la commande de poursuite.
//
//*  Avertissement : 
//
//	* As, B et C DOIVENT être de dimensions correctes.
//	* Le système DOIT être stable (la stabilité est laissée à la charge de l'utilisateur).
//

function [n] = compute_n(As,B,C)

	k = 0;

	while(C*As^(k)*B == 0)
		k = k+1;
	end

	n = k+1;

endfunction
