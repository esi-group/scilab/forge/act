//*  acker_coef.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Méthode d'ackerman pour la stabilisation d'un système Mono-entrée / Mono-sortie
// (SISO).
//
//*  Arguments :
//
//	* A : Matrice de dynamique (n*n)
//	* B : Matrice de commande (n*1)
//	* alpha : Vecteur des coefficients du polynôme caractéristique désirés.
//		Le premier éléments est le coefficient associé à s^0 (la constante).
//
//*  Retour :
//
//	* K : Commande de stabilisation.
//
//* Avertissement :
//
//	* A, B et alpha DOIVENT être de dimensions correctes.
//	* Le système DOIT être commandable (contrôlable).
//	* Le système ne DOIT contenir qu'une entrée et qu'une sortie (SISO).
//	* NE PAS mettre le 1 correspondant à s^n dans alpha.
//

function [K] = acker_coef(A,B,alpha)

	sa = size(A);
	n = sa(1);	// récupération de la dimension du système.

	Com = cont_mat(A,B);
	Comi = inv(Com);
	
	alpha = [alpha,1];

	alphaC = alpha(1) * eye(n,n);

	for i = 2:n+1
		alphaC = alphaC + alpha(i) * A^(i-1);
	end

	K = [zeros(1,n-1),1]*Comi*alphaC;

endfunction

