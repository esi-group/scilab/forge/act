//*  fill_mat.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Ajoute des colones nulles à B de tel sorte qu'elle ait autant de colones
// qu'il y a d'entrée (ni) et ajoute des lignes nulles à C de tel sorte qu'elle
// ait autant de ligne qu'il y a de sortie (no).
//
//* Arguments :
//
//	B : Matrice de commande (n*p)
//	C : Matrice d'observation (q*n)
//
//* Retours :
//
//	B : Matrice B modifiée (ou pas)
//	C : Matrice C modifiée (ou pas)
//
//* Avertissement :
//
//	* B et C DOIVENT être de dimensions correctes.
//	* ni et no DOIVENT être strictement positifs (non-nuls)
//

function [B,C] = fill_mat(B,C,ni,no)

	order = size(B)
	order = order(1)	

	if (no < ni) then
		C = [C ; zeros(ni-no,ordre)]
	elseif (ni < no) then
		B = [B, zeros(ordre,no-ni)]
	end

endfunction
