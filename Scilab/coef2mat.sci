//*  coef2mat.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Calcul les matrices A, B et C du système linéaire à partir des coefficients
// du dénominateur de la fonction de transfère (en s).
//	Le dénominateur doit être de la forme den = s^n + a_{n-1} s^{n-1} ... + a_1 s + a_0
//
//*  Argument :
//
//	coef : coefficient du dénominateur de la fonction de transfère.
//		La liste ne contient pas le coefficient 1 associé à s^n.
//
//*  Retours :
//
//	* A : matrice de dynamique (n*n)
//	* B : matrice de commande (n*1)
//	* C : matrice d'observation (1*n)
//
//*  Avertissement :
//
//	* coef DOIT être de dimension correcte.
//	* Le coefficient (1) associé à s^n (du dénominateur) NE DOIT PAS être présent.
//
//* Déclaration nécessaire :
//
//	* poly2mat.sci
//

function [A,B,C] = coef2mat(coef)

	s = poly(0,'s');
	p = poly([coef,1],'s');

	[A,B,C] = poly2mat(p);

endfunction


