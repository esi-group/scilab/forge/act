//*  pursuit_NINO.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Calcul de la commande de poursuite d'un système à N-entrées / N-sorties.
//  	On désire une poursuite tel que l'erreur e = y - yd soit de la forme :
//  e^(n) = lambda(1) * e + lambda(2) * e^(2) + ... + lambda(i) * e^(i) + ... lambda(n-1) * e^(n-1)
//        = sum(i = 1:n-1, lambda(i) * e^(i) )
//  Avec e^(i) l'erreur dérivée i fois.
//
//	On désire une commande de la forme
//
//           [ (			...			 )   (			   ...			       ) ]
// up = F^-1 [ ( ydi^(ni) + sum(k = 0:ni-1, lambda(i,k)*ydi^(k)) ) - ( Ci As^ni + sum(k = 0:ni-1, lambda(i,k)*Ci*As^k) ) ]
//           [ (			...			 )   (			   ...			       ) ]
//
//*  Arguments :
//
//	* As : Matrice de dynamique stable (n*n)
//	* B : Matrice de commande (n*N)
//	* C : Matrice d'observation (N*n)
//	* lambda : Coefficient d'erreur (N*(max(ni)-1))
//
//*  Retour :
//
//	* Cx : Coefficient de x = - (F^-1 * (C As^ni + sum(k = 0:ni-1, lambda(i,k) Ci As^k) )
//	* Cyd : Coefficient des yd^(i) : Cyd(i) = F^-1 * lambda(i,k)
//	* err : Non nul en cas d'erreur.
//
//* Avertissement :
//
//	* As, B et C DOIVENT être de dimensions correctes.
//	* As DOIT être stable (la stabilisation est laissé à la charge de l'utilisateur)
//	* lambda DOIT être de dimensions correctes : (N *(max(ni)-1)).
//

function [Cx,Cyd,err] = pursuit_NINO(As,B,C,lambda)

	N = size(C);
	N = N(1);

	Cx = 0;
	Cyd = 0;
	err = 0;

	// Calcul des dérivée minimal nécessaire
	n = zeros(1,N)
	for i = 1:N
		n(i) = compute_n(As,B,C(i,:));
	end
	nmax = max(n);

	F = zeros(N,N);
	for i = 1:N
		for j = 1:N
			F(i,j) = C(i,:)*(As^(n(i)-1))*B(:,j)
		end
	end

	if (det(F) == 0) then	// On renvoie une erreur (err = -1) si la 
		err = -1;	// matrice F n'est pas inversible.
		return;
	end

	iF = inv(F);


	// Calcul de Cx(i) : 
	Cx = []
	for i = 1:N
		Cxi = C(i,:)*(As^n(i));
		for k = 0:n(i)-1
			Cxi = Cxi + (lambda(i,k+1) * C(i,:) * A^k);
		end
		Cx = [Cx ; Cxi];
	end
	Cx = -(iF * Cx)

	// Calcul de Cyd :

	Cyd = [lambda,zeros(N,1)]
	for i = 1:N
		Cyd(i,n(i)) = 1
	end
	Cyd = iF * Cyd;

endfunction

