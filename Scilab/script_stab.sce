//*  script_stab.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Script de stabilisation.
//

	cd Scilab/

// Récupération des données

	s = poly(0,'s');
	exec("data_stab.sce");

// Conversion en matrice

	exec("sys2mat.sce");

// Détection de la méthode

	staberr = 0;

	if converr == 0 then
		if exists("StabMethod") == 1 then
			if (StabMethod == 0) then
				// Erreur : Ce n'est pas une méthode valide.
				staberr = 30;
		
			elseif (StabMethod > 0)
				K = ppol(Ai,Bi,stabpole);
				As = (Ai - Bi*K);
			else
				// Erreur : La méthode demandée n'existe pas.
				staberr = 20;
			end
		else
			// Erreur : Le fichier de donnée est erronnée (StabMethod inexistant).
			staberr = 10;
		end
	end

// Écriture des résultats dans un fichier.
	fd_w = file("open","dataSout.sce","new");

	fprintf(fd_w,"error = " + mat2str(converr + staberr) + "\n");
	fprintf(fd_w,"As = " + mat2str(As) + "\n");
	fprintf(fd_w,"Bs = " + mat2str(Bi) + "\n");
	fprintf(fd_w,"Cs = " + mat2str(Ci) + "\n");
	fprintf(fd_w,"K = " + mat2str(K) + "\n");

	file("close",fd_w);

	fd_w = file("open","dataSout.end", "new");
	fprintf("it works !");
	file("close",fd_w);
    	quit;









