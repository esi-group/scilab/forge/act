//*  script_simstab.sce
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Script de stabilisation.
//

	cd Scilab/

// Récupération des données

	s = poly(0,'s');
	exec("data_simstab.sce");

// Conversion en matrice

	exec("sys2mat.sce");

// Affichage

	sA = size(Ai);
	sysorder = sA(1);

	sB = size(Bi);
	ni = sB(2);
	
	sC = size(Ci);
	no = sC(1);

	signalstart = 1;

	if exists("SimInput") == 1 then
		if SimInput == CST_SIM_STEP then
			function [u] = U(t)
				if t < signalstart then
					u = zeros(ni,1);
				else
					u = amp * ones(ni,1);
				end
			endfunction

		elseif SimInput == CST_SIM_DIRAC then
			duree = 20*step;
			function [u] = U(t)
				if (t < signalstart) | (t > (signalstart + duree)) then
					u = zeros(ni,1);
				else
					u = amp * ones(ni,1);
				end
			endfunction

		elseif SimInput == CST_SIM_RAMP then
			function [u] = U(t)
				if t < signalstart then
					u = zeros(ni, 1);
				else
					u = amp * (t-signalstart) * ones(ni,1);
				end
			endfunction

		elseif
			// Erreur : on affiche rien
			quit;
		end
	else
		// Erreur : on affiche rien
		quit;
	end

	X0 = zeros(sysorder,1);
	t = [0:step:tf];

	ni

	

	for i=1:ni
		clear f
		
		Usel = zeros(ni, ni);
		Usel(:,i) = 1;

		function [dX] = f(t,X)
			dX = Ai * X + Bi * Usel * U(t);
		endfunction

		printf("ode : start\n")

		[Xo] = ode(X0,0,t,f);

		printf("ode : end\n")

		if typeout == 0 then
			Y = Xo(nout,:);
		else
			Yo = Ci * Xo;
			Y = Yo(nout,:);
		end

		command = zeros(ni,length(t));
		for j = 1:length(t)
			command(:,j) = U(t(j));
		end

		f = figure();
		plot(t,Y);
		plot(t,command,"r");
	end

	while length(winsid()) ~= 0,	// tant qu'il y a des figures ouvertes
	end
	





