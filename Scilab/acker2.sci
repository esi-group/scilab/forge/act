//* Avertissement :
//
//	* A, B et p DOIVENT être de dimensions correctes.
//	* Le système DOIT être commandable (contrôlable).
//	* Le système ne DOIT contenir qu'une entrée et qu'une sortie (SISO).
//
//* Déclaration nécessaire :
//
//	* acker_coef.sci
//

function [K] = acker(A,B,p)

	equation = poly(p,"s","roots");

	alpha = coeff(equation);
	
	K = acker_coef(A,B,alpha);

endfunction