//*  pursuit_SISO.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Calcul de la commande de poursuite d'un système mono-entrée / mono-sortie.
//  	On désire une poursuite tel que l'erreur e = y - yd soit de la forme :
//  e^(n) = lambda(1) * e + lambda(2) * e^(2) + ... + lambda(i) * e^(i) + ... lambda(n-1) * e^(n-1)
//        = sum(i = 1:n-1, lambda(i) * e^(i) )
//  Avec e^(i) l'erreur dérivée i fois.
//
//	La commande de poursuite up est de la forme :
//		up = (C As^{n-1} B)^-1 ( -(C As^n + sum(i = 0:n-1, lambda(i) C As^i))x + yd^(n) + sum(i = 0:n-1, lambda(i) yd^(i) ) 
//
//*  Arguments :
//
//	* As : Matrice de dynamique stable (n*n)
//	* B : Matrice de commande (n*1)
//	* C : Matrice d'observation (1*n)
//	* lambda : Coefficient d'erreur
//
//*  Retour :
//
//	* Cx : Coefficient de x = - (C As^{n-1} B)^-1 * (C As^n + sum(i = 0:n-1, lambda(i) C As^i) )
//	* Cyd : Coefficient des yd^(i) : Cyd(i) = (C As^{n-1} B)^-1 * lambda(i)
//	* err : Non nul en cas d'erreur.
//
//* Avertissement :
//
//	* As, B et C DOIVENT être de dimensions correctes.
//	* As DOIT être stable (la stabilisation est laissé à la charge de l'utilisateur)
//	* lambda DOIT être de dimensions correctes : n-1.
//	* Ne pas mettre de lambda(n).
//

function [Cx,Cyd,err] = pursuit_SISO(As,B,C,lambda)

	Cx = 0;
	Cyd = 0;
	err = 0;

	// Calcul de la dérivée minimal nécessaire
	n = compute_n(As,B,C);

	F = C*(As^(n-1))*B;
		// F : coefficient général
		//	Généralisable en matrice de découplage pour le MIMO.

	if (det(F) == 0) then	// On renvoie une erreur (err = -1) si le
		err = -1;	// coefficient est nul.
		return;
	end

	iF = inv(F);


	// Calcul de Cx : 

	Cx = C * (As^n);
	for i = 0:n-1
		Cx = Cx + lambda(i+1) * C * (As^i);
	end
	Cx = -(iF * Cx)

	// Calcul de Cyd :

	Cyd = [lambda,1];
	Cyd = iF * Cyd;

endfunction

