//*  check_mat.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Vérifie si les dimensions des matrices sont cohérentes entre elles.
//	Dans le même temps la fonction renvois des informations sur les
// dimensions du système.
//	En cas de problème la fonction retourne un code d'erreur (-1) sur :
//		- ordre : si A n'est pas carré.
//		- ne : si le nombre de lignes de B est différent de l'ordre du système.
//		- ns : si le nombre de colones de C est différent de l'ordre du système.
//
//*  Arguments :
//
//	* A : Matrice de dynamique
//	* B : Matrice de commande
//	* C : Matrice d'observation
//
//*  Retours :
//
//	* order : ordre du système ou -1 en cas d'erreur sur A.
//	* ni : nombre d'entrée ou -1 en cas d'erreur sur B.
//	* no : nombre de sortie ou -1 en cas d'erreur sur C.
//

function [order,ni,no] = check_mat(A,B,C)

	sa = size(A);
	sb = size(B);
	sc = size(C);

	order = sa(1);
	ni = sb(2);
	no = sc(1);

	
	if (sa(1)<>sa(2)) then	// Si A n'est pas carré
		order = -1;
	end

	if (sa(2)<>sb(1)) then 	// Si B n'a pas le bon nombre de ligne
		ni = -1;
	end

	if (sa(1)<>sc(2)) then 	// Si C n'a pas le bon nombre de colone
		no = -1;
	end

endfunction


