//*  acker_alphas.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Méthode d'ackerman pour la stabilisation d'un système Mono-entrée / Mono-sortie
// (SISI).
//
//*  Arguments :
//
//	* A : Matrice de dynamique (n*n)
//	* B : Matrice de commande (n*1)
//	* a : Coefficients de la fonction de transfert
//
//*  Retour :
//
//	* K : Commande de stabilisation.
//
//* Avertissement :
//
//	* A, B et a DOIVENT être de dimensions correctes.
//	* Le système DOIT être commandable (contrôlable).
//	* Le système ne DOIT contenir qu'une entrée et qu'une sortie (SISO).
//	* a = [a0,a1,....,an] avec D(s) = a0 + a1.s + a2.s² + ... + an.s^n
//

function [K] = acker_poly(A,B,a)

	s = poly(0,'s');

	alpha = coeff(a);
	
	K = acker_coef(A,B,alpha);

endfunction

