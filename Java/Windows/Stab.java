/*	Stab.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page contenant la fenêtre StabStd et lançant le calcul des nouvelles matrices du système stabilisé à l'aide de Scilab.
 */

package Windows;

import Windows.StabStd;
import Windows.PlotParam;
import Windows.Pursuit;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

import PropertyFile.i18n;

public class Stab extends JPanel {

	private JLabel Type;
	private JLabel hspace;
	private JComboBox combo;
	private StabStd std;
	private JButton visu;

	private Pursuit purs;
	private PlotParam plotParam;
	private String plot;

	private i18n lang;

	private CloudData cd;

	public Stab(i18n fichierlang, CloudData cloud, Pursuit pursuit) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.purs = pursuit;
		this.initComponent();

		this.setLayout(new BorderLayout());

		combo.setPreferredSize(new Dimension(300,20));
		combo.addActionListener(new ItemAction());
		combo.addItem("SISO (Ackermann)");
		combo.addItem("MIMO (Brunovsky)");

		JPanel top = new JPanel();
		top.add(Type);
		top.add(hspace);
		top.add(combo);

		this.add(top, BorderLayout.NORTH);
		this.add(std, BorderLayout.CENTER);
		this.add(visu, BorderLayout.SOUTH);

		visu.addActionListener(new PlotListener());
	}

	/**
	*	Initialise la classe Stab
	*/
	public void initComponent() {
		
		this.Type = new JLabel(this.lang.getString("method"));
		this.hspace = new JLabel("      ");
		this.combo = new JComboBox();
		this.std = new StabStd(lang, cd, purs);
		this.visu = new JButton(this.lang.getString("display"));
		this.plot = this.lang.getString("display");
	}

	/**
	*	Réinitialise la classe Stab lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;
		
		this.Type.setText(this.lang.getString("method"));
		this.std.reinitComponent(lang);
		this.visu.setText(this.lang.getString("display"));
		this.plot = this.lang.getString("display");
	}


	public class PlotListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			plotParam = new PlotParam(null, plot, true, lang, cd);
			plotParam.requestFocusInWindow();
		}
	}

        class ItemAction implements ActionListener{
 
                public void actionPerformed(ActionEvent e) {

			if(combo.getSelectedItem() == "SISO (Ackermann)") {
				cd.saveStabMethod(2);
			}
			else if(combo.getSelectedItem() == "MIMO (Brunovsky)") {
				cd.saveStabMethod(3);
			}
                }               
        }

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		std.open();
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		std.save();
	}
}


