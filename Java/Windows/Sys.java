/*	Sys.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page contenant les fenêtres SysAlpha, Sysq, SysH, SysMat et SysPole, donc la fenêtre dans laquelle on entre les données du système.
 */

package Windows;

import Windows.SysMat;
import Windows.SysH;
import Windows.SysPole;
import Windows.SysAlpha;
import Windows.SysEq;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

import PropertyFile.i18n;

public class Sys extends JPanel implements ItemListener {

	int i;
	private JLabel Type;
	private JLabel hspace;
	private JButton okBouton;
	SysMat Mat;
	SysH H;
	SysPole Pole;
	SysAlpha Alpha;
	SysEq Eq;
	JPanel card;
	String a;
	String b;
	String c;
	String d;
	String e;
	JComboBox box;
	JPanel top;

	private i18n lang;

	private CloudData cd;

	public Sys(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();
		String[] s = {a,b,c,d,e};

		this.setLayout(new BorderLayout());

		this.box = new JComboBox(s);
		this.box.setPreferredSize(new Dimension(300,20));
		this.box.addItemListener(this);

		this.top = new JPanel();
		this.top.add(Type);
		this.top.add(hspace);
		this.top.add(box);

		this.card = new JPanel(new CardLayout());
		this.card.add(Mat, a);
		this.card.add(H, b);
		this.card.add(Pole, c);
		this.card.add(Alpha, d);
		this.card.add(Eq, e);


		JPanel control = new JPanel();
		this.okBouton = new JButton(this.lang.getString("Val"));
		
		okBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {			
				if(i==0) Mat.set();
				else if(i==1) H.set();
				else if(i==2) Pole.set();
				else if(i==3) Alpha.set();
				else if(i==4) Eq.set();
				setVisible(false);
			}		
		});

		control.add(okBouton);


		this.add(top, BorderLayout.NORTH);
		this.add(card, BorderLayout.CENTER);
		this.add(control, BorderLayout.SOUTH);

	}

	/**
	*	Initialise la classe Sys
	*/
	public void initComponent() {

		this.i = 0;

		this.Type = new JLabel(this.lang.getString("type_inputs"));
		this.hspace = new JLabel("      ");
		this.Mat = new SysMat(this.lang, cd);
		this.H = new SysH(this.lang, cd);
		this.Pole = new SysPole(this.lang, cd);
		this.Alpha = new SysAlpha(this.lang, cd);
		this.Eq = new SysEq(this.lang, cd);
		
		
		this.a = this.lang.getString("matrix");
		this.b = this.lang.getString("transfer_function");
		this.c = this.lang.getString("poles2");
		this.d = this.lang.getString("alphas2");
		this.e = this.lang.getString("equations2");


	}

	/**
	*	Réinitialise la classe Sys lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.Type.setText(this.lang.getString("type_inputs"));
		this.Mat.reinitComponent(this.lang);
		this.H.reinitComponent(this.lang);
		this.Pole.reinitComponent(this.lang);
		this.Alpha.reinitComponent(this.lang);
		this.Eq.reinitComponent(this.lang);

		this.a = this.lang.getString("matrix");
		this.b = this.lang.getString("transfer_function");
		this.c = this.lang.getString("poles2");
		this.d = this.lang.getString("alphas2");
		this.e = this.lang.getString("equations2");

		this.box.removeAllItems();
		this.box.addItem(this.a);
		this.box.addItem(this.b);
		this.box.addItem(this.c);
		this.box.addItem(this.d);
		this.box.addItem(this.e);

		this.card.removeAll();

		this.card.add(Mat, a);
		this.card.add(H, b);
		this.card.add(Pole, c);
		this.card.add(Alpha, d);
		this.card.add(Eq, e);

		this.okBouton.setText(this.lang.getString("Val"));
		
	}

	/**
	*	Gère les changement de choix de l'utilisateur en ce qui concerne
	* la méthode pour entrer le système
	*
	* @param e
	*	ItemEvent
	*/
	public void itemStateChanged(ItemEvent ie) {
		CardLayout cl = (CardLayout)(card.getLayout());
		cl.show(card, (String)ie.getItem());
		if(ie.getItem() == a) this.i = 0;
		else if(ie.getItem() == b) this.i = 1;
		else if(ie.getItem() == c) this.i = 2;
		else if(ie.getItem() == d) this.i = 3;
		else if(ie.getItem() == e) this.i = 4;
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		Mat.open();
		H.open();
		Pole.open();
		Alpha.open();
		Eq.open();
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		Mat.save();
		H.save();
		Pole.save();
		Alpha.save();
		Eq.save();
	}





/*	public SysMat getMat() {
		return Mat;
	}
	public SysH getH() {
		return H;
	}*/

 /*       class ItemAction implements ActionListener{
 
                public void actionPerformed(ActionEvent e) {

			if(box.getSelectedItem() == "Matrices") {
				remove(H);
				add(Mat, BorderLayout.CENTER);
				repaint();
			}
			else if(box.getSelectedItem() == "Fonction de transfert") {
				remove(Mat);
				add(H, BorderLayout.CENTER);
				repaint();
			}
                }               
        }*/
}


