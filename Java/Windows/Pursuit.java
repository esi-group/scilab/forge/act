/*	Pursuit.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page contenant la fenêtre PursuitStd et lançant le calcul des nouvelles matrices du système pour le suivi de trajectoire à l'aide de Scilab.
 */

package Windows;

import Windows.PursuitStd;
import Windows.PlotParam;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

import PropertyFile.i18n;

public class Pursuit extends JPanel {

	private JPanel tab;
	private JPanel tab1;
	private JLabel Type;
	private JLabel hspace;
	private JComboBox combo;
	private JCheckBox check;
	private PursuitStd std;
	private JButton visu;

	private PlotParam plotParam;
	private String plot;

	private i18n lang;

	private CloudData cd;

	public Pursuit(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new BorderLayout());

		combo.setPreferredSize(new Dimension(300,20));
		combo.addActionListener(new ItemAction());
		combo.addItem("SISO");
		combo.addItem("MIMO");

		tab1.add(Type);
		tab1.add(hspace);
		tab1.add(combo);

		tab.setLayout(new GridLayout(2,1));
		tab.add(tab1);
		tab.add(check);

		JPanel top = new JPanel();
		top.add(tab);


		this.add(top, BorderLayout.NORTH);
		this.add(std, BorderLayout.CENTER);
		this.add(visu, BorderLayout.SOUTH);

		visu.addActionListener(new PlotListener());
	}

	/**
	*	Initialise la classe Pursuit
	*/
	public void initComponent() {

		this.tab = new JPanel();
		this.tab1 = new JPanel();
		this.Type = new JLabel(this.lang.getString("method"));
		this.hspace = new JLabel("      ");
		this.combo = new JComboBox();
		this.check = new JCheckBox(this.lang.getString("use_syst_stab"));
		this.check.setEnabled(false);
		this.check.setSelected(true);
		this.std = new PursuitStd(lang, cd, check);
		this.visu = new JButton(this.lang.getString("display"));
		this.plot = this.lang.getString("display");
	}

	/**
	*	Réinitialise la classe Pursuit lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.Type.setText(this.lang.getString("method"));
		this.check.setText(this.lang.getString("use_syst_stab"));
		this.std.reinitComponent(lang);
		this.visu.setText(this.lang.getString("display"));
		this.plot = this.lang.getString("display");
	}


	public class PlotListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			plotParam = new PlotParam(null, plot, true, lang, cd);
			plotParam.requestFocusInWindow();
		}
	}

        class ItemAction implements ActionListener{
 
                public void actionPerformed(ActionEvent e) {

			if(combo.getSelectedItem() == "SISO") {
				cd.savePursuitMethod(1);
			}
			else if(combo.getSelectedItem() == "MIMO") {
				cd.savePursuitMethod(1);
			}
                }               
        }

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		std.open();
		if(cd.getUseStab() == true) {
			this.check.setSelected(true);
		}
		else {
			this.check.setSelected(false);
		}

		if(cd.isStabilized() == true) {
			this.check.setEnabled(true);
		}
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		std.save();
	}

	public void checkEnabled() {
		this.check.setEnabled(true);
	}
}


