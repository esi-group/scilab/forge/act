/*	PlotParam.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fenêtre permettant de choisir les paramètres de tracé de courbes.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
import static java.lang.Math.*;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JDialog;

import PropertyFile.i18n;

public class PlotParam extends JDialog {

	private JPanel panTexte1, panTexte2, panTexte3, panTexte4;
	private JLabel iniTimeLabel, finalTimeLabel, footStepLabel, state, input, response, parametre;
	private JTextField iniTime, finalTime, footStep, param;
	private ArrayList<JRadioButton> x;
	private ArrayList<JRadioButton> y;
	private JRadioButton impulse, step, ramp;
	private ButtonGroup plotgroup1 = new ButtonGroup();
	private ButtonGroup plotgroup2 = new ButtonGroup();
	private String toPlot, resp, respParam, State;
	private int cstSim;

	private i18n lang;

	private CloudData cd;

	public PlotParam(JFrame parent, String title, boolean modal, i18n fichierlang, CloudData cloud) {
		super(parent, title, modal);
		this.setSize(700, 360);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.lang = fichierlang;
		this.cd = cloud;
		cstSim = cd.CST_SIM_NULL;
		this.initComponent();
	}

	/**
	*	Initialise la classe PlotParam
	*/
	private void initComponent(){

		x = new ArrayList<JRadioButton>();
		y = new ArrayList<JRadioButton>();

		this.panTexte1 = new JPanel();
		this.panTexte1.setBackground(Color.white);
		this.panTexte1.setPreferredSize(new Dimension(300, 70));
		this.panTexte2 = new JPanel();
		this.panTexte2.setBackground(Color.white);
		this.panTexte2.setPreferredSize(new Dimension(300, 70));
		this.panTexte3 = new JPanel();
		this.panTexte3.setBackground(Color.white);
		this.panTexte3.setPreferredSize(new Dimension(300, 100));

		int i,j;
		for(i=0;i<cd.getSysOrder();i++) {
			JRadioButton xi = new JRadioButton("x"+((Integer)(i+1)).toString());
			x.add(xi);
			x.get(i).addActionListener(new StateOutputListener());
			this.panTexte1.add(x.get(i));
			this.plotgroup1.add(xi);
		}
		for(j=0;j<cd.getNoutputs();j++) {
			JRadioButton yi = new JRadioButton("y"+((Integer)(j+1)).toString());
			y.add(yi);
			y.get(j).addActionListener(new StateOutputListener());
			this.panTexte2.add(y.get(j));
			this.plotgroup1.add(yi);
		}

		this.impulse = new JRadioButton(this.lang.getString("impulse"));
		this.impulse.addActionListener(new AnswerListener());
		this.panTexte3.add(impulse);
		this.plotgroup2.add(impulse);
		this.step = new JRadioButton(this.lang.getString("step"));
		this.step.addActionListener(new AnswerListener());
		this.panTexte3.add(step);
		this.plotgroup2.add(step);
		this.ramp = new JRadioButton(this.lang.getString("ramp"));
		this.ramp.addActionListener(new AnswerListener());
		this.panTexte3.add(ramp);
		this.plotgroup2.add(ramp);
		this.parametre = new JLabel(lang.getString("high"));
		this.param = new JTextField();
		this.param.setPreferredSize(new Dimension(50, 30));
		this.panTexte3.add(parametre);
		this.panTexte3.add(param);

		//Texte et zones de texte
		this.panTexte4 = new JPanel();
		this.panTexte4.setBackground(Color.white);
		this.panTexte4.setPreferredSize(new Dimension(600, 80));
		this.iniTime = new JTextField();
		this.iniTime.setPreferredSize(new Dimension(50, 30));
		this.finalTime = new JTextField();
		this.finalTime.setPreferredSize(new Dimension(50, 30));
		this.footStep = new JTextField();
		this.footStep.setPreferredSize(new Dimension(50, 30));
		this.panTexte1.setBorder(BorderFactory.createTitledBorder(this.lang.getString("state")));
		this.panTexte2.setBorder(BorderFactory.createTitledBorder(this.lang.getString("output")));
		this.panTexte3.setBorder(BorderFactory.createTitledBorder(this.lang.getString("response")));
		this.panTexte4.setBorder(BorderFactory.createTitledBorder(this.lang.getString("timeParam")));
		//lignesLabel = new JLabel("Lignes :");
		this.iniTimeLabel = new JLabel(this.lang.getString("iniTime"));
		this.finalTimeLabel = new JLabel(this.lang.getString("finalTime"));
		this.footStepLabel = new JLabel(this.lang.getString("footStep"));
		this.panTexte4.add(iniTimeLabel);
		this.panTexte4.add(iniTime);
		this.panTexte4.add(finalTimeLabel);
		this.panTexte4.add(finalTime);
		this.panTexte4.add(footStepLabel);
		this.panTexte4.add(footStep);



		JPanel content = new JPanel();
		content.setBackground(Color.white);
		content.add(panTexte1);
		content.add(panTexte2);
		content.add(panTexte3);
		content.add(panTexte4);
		
		JPanel control = new JPanel();
		JButton okBouton = new JButton(this.lang.getString("Ok"));
		
		okBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				try {
					cd.doSimStab(iniTime.getText(), finalTime.getText(), footStep.getText(), param.getText(), State, cstSim);
				}
				catch(CloudDataException e1) {
					JOptionPane jop1 = new JOptionPane();
					jop1.showMessageDialog(null, e1.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
				}
			}		
		});
		
		JButton cancelBouton = new JButton(this.lang.getString("Cancel"));
		cancelBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}			
		});
		
		control.add(okBouton);
		control.add(cancelBouton);

		this.getContentPane().add(content, BorderLayout.CENTER);
		this.getContentPane().add(control, BorderLayout.SOUTH);

		this.setVisible(true);

	}

	public class StateOutputListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			JRadioButton source = (JRadioButton) e.getSource();

			State = ((JRadioButton)source).getLabel();
		}
	}

	public class AnswerListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			JRadioButton source = (JRadioButton) e.getSource();

			resp = ((JRadioButton)source).toString();

			if( ((JRadioButton)source).getLabel() == lang.getString("impulse") ) {
				parametre.setText(lang.getString("high"));
				cstSim = cd.CST_SIM_DIRAC;
			}
			else if( ((JRadioButton)source).getLabel() == lang.getString("step") ) {
				parametre.setText(lang.getString("high"));
				cstSim = cd.CST_SIM_STEP;
			}
			else if( ((JRadioButton)source).getLabel() == lang.getString("ramp") ) {
				parametre.setText(lang.getString("gradient"));
				cstSim = cd.CST_SIM_RAMP;
			}
			repaint();
		}
	}
}


