/*	AssGrid.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Tableau central de l'assistant d'écriture de matrices.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
import static java.lang.Math.*;
import static java.lang.Double.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;

import java.lang.Double.*;

import java.util.ArrayList;

import PropertyFile.i18n;
import SciMatrix.SciMatrix;

public class AssGrid extends JPanel {

	private ArrayList<JTextField> Mat;
	private JLabel text;

	private i18n lang;

	private CloudData cd;

	private int rows;
	private int columns;
	private JTextField matrice;

	public AssGrid(int a, int b, JTextField mat, i18n fichierlang, CloudData cloud) {

		this.rows = a;
		this.columns = b;
		this.matrice = mat;
		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();


			//"Matrice : "
		c.fill = GridBagConstraints.VERTICAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = (a-1)/2;
		//c.gridheight = a;
		this.add(text,c);

		int k;
		for(k=0;k<a*b;k++) {
			Mat.add(k, new JTextField());
			Mat.get(k).setPreferredSize(new Dimension(40, 40));
		}

		int i,j;
		for(i=0;i<a;i++) {

			for(j=0;j<b;j++) {

					//Zones te teste
				c.fill = GridBagConstraints.HORIZONTAL;
				c.weightx = 1.0;
				c.weighty = 1.0;
				c.gridx = j+1;
				c.gridy = i;
				c.gridwidth = 1;
				this.add(Mat.get(i*b+j),c);
			}
		}
	}

	/**
	* 	Initialise la classe AssGrid
	*/
	public void initComponent() {

		this.Mat = new ArrayList<JTextField>();
		this.text = new JLabel(this.lang.getString("matrix2"));

	}

	/**
	* 	Transforme les données tu tableau de AssGrid
	* en matrice formalisée Scilab, l'écrit dans la
	* zone te texte et l'enregistre dans CloudData.
	*/
	public void transform() {

		SciMatrix p = new SciMatrix();

		p.setRows(this.rows);
		p.setColumns(this.columns);

		int i;
		for(i=0;i<this.rows*this.columns;i++) {
			try {
				p.add(parseDouble(Mat.get(i).getText()));
			}
			catch(NumberFormatException e) {
				e.printStackTrace();
			}
		}

		this.matrice.setText(p.toString());
	}
}

