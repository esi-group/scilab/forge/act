/*	SysPole.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page permettant d'entrer les pôles du système.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import PropertyFile.i18n;


public class SysPole extends JPanel {

	private JLabel poles;
	private JLabel intro;
	private JLabel ex;
	private JLabel hspace;
	private JTextField Poles;

	private i18n lang;

	private CloudData cd;

	public SysPole(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

			//espace ligne supplémentaire
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 6.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 6;
		this.add(hspace,c);




			//espace de début d'introduction
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(hspace,c);

			//introduction
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 5;
		this.add(intro,c);



			//Texte: "Pôles: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(poles,c);

			//zone de texte: Pôles
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(Poles,c);

			//espace fin de 1ere ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(hspace,c);



			//espace de début d'exemple
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(hspace,c);

			//exemple
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 5;
		this.add(ex,c);



			//espace ligne supplémentaire
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 2.0;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 6;
		this.add(hspace,c);
	}

	/**
	*	Initialise la classe SysPole
	*/
	public void initComponent() {

		this.poles = new JLabel(this.lang.getString("poles"));
		this.intro = new JLabel(this.lang.getString("enter_poles"));
		this.ex = new JLabel(this.lang.getString("ex"));
		this.hspace = new JLabel(" ");
		this.Poles = new JTextField();

	}

	/**
	*	Réinitialise la classe SysPole lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.poles.setText(this.lang.getString("poles"));
		this.intro.setText(this.lang.getString("enter_poles"));
		this.ex.setText(this.lang.getString("ex"));
	}

	/**
	*	Enregistre les données entrées par l'utilisateur dans la classe CloudData
	* pour pouvoir les traiter et lancer les calculs Scilab ou Maxima
	*/
	public void set() {

		try {
			this.cd.setInputPoles(Poles.getText());
		}
		catch (CloudDataException e) {
			JOptionPane jop = new JOptionPane();
			jop.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		Poles.setText(cd.getOriginalPoles());
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		cd.saveInputPoles(Poles.getText());
	}
}


