/*	SysMat.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page permettant d'entrer le système sous forme de matrices d'état (A, B, C).
 */

package Windows;

import Windows.DimMat;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import PropertyFile.i18n;

public class SysMat extends JPanel
{
	private JLabel A;
	private JLabel B;
	private JLabel C;
	private JLabel eq1;
	private JLabel eq2;

	private JLabel hspace;

	private JTextField MatA;
	private JTextField MatB;
	private JTextField MatC;

	private JButton bouton1;
	private JButton bouton2;
	private JButton bouton3;

	private DimMat dimMat;
	private String dim;

	private i18n lang;

	private CloudData cd;

	public SysMat(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();


			//Texte: "A: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(A,c);

			//Zone de texte: A
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 10.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(MatA,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(hspace,c);

			//Bouton "Assistant" 1
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 6;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(bouton1,c);

			//Texte: "B: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(B,c);

			//Zone de texte: B
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 10.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 4;
		this.add(MatB,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(hspace,c);


			//Bouton "Assistant" 2
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 6;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(bouton2,c);

			//Texte: "C: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(C,c);

			//Zone de texte: C
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 10.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(MatC,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(hspace,c);


			//Bouton "Assistant" 3
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 6;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(bouton3,c);



			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 0.5;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 2;
		this.add(hspace,c);

			//example équation d'état
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 0.5;
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 5;
		this.add(eq1,c);


			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 0.5;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 2;
		this.add(hspace,c);

			//example équation d'état
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 0.5;
		c.gridx = 2;
		c.gridy = 4;
		c.gridwidth = 5;
		this.add(eq2,c);

		bouton1.addActionListener(new AssListener());
		bouton2.addActionListener(new AssListener());
		bouton3.addActionListener(new AssListener());

	}

	/**
	*	Initialise la classe SysMat
	*/
	public void initComponent() {

		this.A = new JLabel("A: ");
		this.B = new JLabel("B: ");
		this.C = new JLabel("C: ");
		this.eq1 = new JLabel("X' = A.X + B.U");
		this.eq2 = new JLabel("Y = C.X");

		this.hspace = new JLabel(" ");

		this.MatA = new JTextField();
		this.MatB = new JTextField();
		this.MatC = new JTextField();

		this.bouton1 = new JButton(this.lang.getString("assistant"));
		this.bouton2 = new JButton(this.lang.getString("assistant"));
		this.bouton3 = new JButton(this.lang.getString("assistant"));

		this.dim = this.lang.getString("dimensions");

	}

	/**
	*	Réinitialise la classe SysMat lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.bouton1.setText(this.lang.getString("assistant"));
		this.bouton2.setText(this.lang.getString("assistant"));
		this.bouton3.setText(this.lang.getString("assistant"));

		this.dim = this.lang.getString("dimensions");

	}


	public class AssListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			JButton source = (JButton) e.getSource();
			if (source == bouton1)		dimMat = new DimMat(MatA, null, dim, true, lang, cd);
			else if (source == bouton2)	dimMat = new DimMat(MatB, null, dim, true, lang, cd);
			else if (source == bouton3)	dimMat = new DimMat(MatC, null, dim, true, lang, cd);
			dimMat.requestFocusInWindow();
		}
	}

	/**
	*	Enregistre les données entrées par l'utilisateur dans la classe CloudData
	* pour pouvoir les traiter et lancer les calculs Scilab ou Maxima
	*/
	public void set() {

		try {
			this.cd.setInputMatrix(MatA.getText(), MatB.getText(), MatC.getText());
		}
		catch (CloudDataException e) {
			JOptionPane jop = new JOptionPane();
			jop.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		MatA.setText(cd.getOriginalMatrixA());
		MatB.setText(cd.getOriginalMatrixB());
		MatC.setText(cd.getOriginalMatrixC());
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		cd.saveInputMatrix(MatA.getText(), MatB.getText(), MatC.getText());
	}
}

