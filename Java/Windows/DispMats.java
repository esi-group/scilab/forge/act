/*	DispMats.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fenêtre d'affichage des Matrices stabilisées.
 */

package Windows;

import Windows.Assistant;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
import static java.lang.Math.*;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JDialog;

import PropertyFile.i18n;


public class DispMats extends JFrame {
	
	private CloudData cd;
	private JTextArea mats;

	public DispMats(JFrame parent, String title, boolean modal, CloudData cloudData) {
		
		this.cd = cloudData;

		this.setLayout(new BorderLayout());
		this.setSize(450, 150);
		//Définit un titre pour votre fenêtre
		this.setTitle(title);
		//Nous allons maintenant dire à notre objet de se positionner au centre
		this.setLocationRelativeTo(null);

		this.initComponent();
	}


	void initComponent() {
		mats = new JTextArea(10,1);
		mats.setText("A = " + cd.getStabA() + "\nB = " + cd.getStabB() + "\nC = " + cd.getStabC() + "\nK = " + cd.getStabK());

		JPanel content = new JPanel();
		content.setBackground(Color.white);
		content.add(mats);

		this.getContentPane().add(content, BorderLayout.CENTER);
		this.setVisible(true);
	}
}








