
import SciMatrix.SciMatrix;
import SciMatrix.SciMatrixException;

public class tstmat
{
	static public void main(String[] args)
	{
		try
		{
		SciMatrix p = new SciMatrix();

		p.setRows(2);
		p.setColumns(2);

		p.add(1.0);
		p.add(2.0);
		p.add(3.0);
		p.add(4.0);

		System.out.println(p);

		System.out.println("\n");

		SciMatrix e = new SciMatrix("4 ,   3; 6,  7;2.5,9");

		System.out.println(e.getColumns() + " " + e.getRows());

		System.out.println(e);

		e = new SciMatrix("4,5;6,7");

		System.out.println(e.getColumns() + " " + e.getRows());

		e = new SciMatrix("4,5,6");

		System.out.println(e.getColumns() + " " + e.getRows());


		}
		catch(SciMatrixException e)
		{
			e.printStackTrace();
		}

	}
}
