/*	SciMatrix.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Allows to convert a matrix into string object and convert a string into a 
 * matrix. Columns must be separated by comma and rows by a semicolon in the string
 * object. Moreover the matrix is written row by row.
 * 	Example : 1,2;3,4
 * This object can also be used to detect problems in the matrix.
 */

package SciMatrix;

import java.util.ArrayList;

/** Allows to convert a matrix into string object and convert a string into a 
 * matrix. Columns must be separated by comma and rows by a semicolon in the string
 * object. Moreover the matrix is written row by row.
 * 	Example : 1,2;3,4
 * This object can also be used to detect problems in the matrix.
 */
public class SciMatrix
{

	/** element list of the matrix. */
	private ArrayList<Double> matrix;

	private int rows;
	private int columns;

	/** Creates a empty SciMatrix object.
	 */
	public SciMatrix()
	{
		columns = 0;
		rows = 0;
		matrix = new ArrayList<Double>();
	}

	/** Initializes a newly created SciMatrix object by a null matrix.
	 *
	 * @param rows the rows number of the matrix.
	 * @param columns the columns number of the matrix.
	 */
	public SciMatrix(int rows, int columns)
	{
		this();
		this.newMatrix(rows,columns);
	}

	/** Initializes a newly created SciMatrix object with a string containing a
	 * matrix representation. Columns must be separated by a comma and rows
	 * by a semicolon. Moreover the matrix is written row by row.
	 * Example : 1,2;3,4
	 *
	 * @param txtmat string containing the matrix representation.
	 *
	 * @throws SciMatrixException if an error occured during the parse operation.
	 */
	public SciMatrix(String txtmat) throws SciMatrixException
	{
		this();
		this.toMatrix(txtmat);
	}

	/** Returns a string object representing this matrix.
	 *
	 * @return string object representing this matrix
	 */
	public String toString()
	{
		String res = "";
		int pos = 0;
		for (int i = 0; i < this.rows ; i++)
		{
			if (i > 0)
				res += ";";
			for(int j = 0; j < this.columns; j++)
			{
				if(j > 0)
					res += ",";
				res += this.matrix.get(i*this.columns + j);
			}
		}
		return res;
	}

	/** Adds an element to the end of the list.
	 *
	 * @param val value to add.
	 */
	public void add(double val)
	{
		this.matrix.add(val);
	}

	/** Redefines the matrix (and his dimensions) with zero as filling value.
	 *
	 * @param rows the rows number of the matrix.
	 * @param columns the columns number of the matrix.
	 */
	public void newMatrix(int rows, int columns)
	{
		this.rows = rows;
		this.columns = columns;
		int tot = this.rows*this.columns;
		for (int i = 0; i < tot; i++)
		{
			this.matrix.add(0.0);
		}
	}

	/** Sets the rows number of the matrix.
	 *
	 * @param rows the rows number of the matrix.
	 */
	public void setRows(int rows)
	{
		this.rows = rows;
	}

	/** Sets the columns number of the matrix.
	 *
	 * @param columns the columns number of the matrix.
	 */
	public void setColumns(int columns)
	{
		this.columns = columns;
	}

	/** Gets the rows number of the matrix.
	 *
	 * @return the rows number of the matrix.
	 */
	public int getRows()
	{
		return this.rows;
	}

	/** Gets the columns number of the matrix.
	 *
	 * @return the columns number of the matrix.
	 */
	public int getColumns()
	{
		return this.columns;
	}
	
	/** Gets the value of a matrix element. The first row and the first
	 * column have the number 0 (and not 1).
	 *
	 * @param row row where is the value to get.
	 * @param column column where is the value to get.
	 *
	 * @return matrix element value.
	 *
	 * @see SciMatrix#getValue(int) getValue(int pos)
	 */
	public double getValue(int row, int column)
	{
		return this.matrix.get(row*this.columns + column);
	}

	/** Sets the value of a matrix element. The first row and the first
	 * column have the number 0 (and not 1).
	 *
	 * @param row row where is the value to set.
	 * @param column column where is the value to set.
	 * @param value new value.
	 *
	 * @see SciMatrix#setValue(int,double) setValue(int pos, double value)
	 */
	public void setValue(int row, int column, double value)
	{
		this.matrix.set(row*this.columns+column,value);
	}

	/** Gets the value of a matrix element.
	 *
	 * @return value at the given position in the list.
	 *
	 * @see SciMatrix#getValue(int, int) getValue(int row, int column)
	 */
	public double getValue(int pos)
	{
		return this.matrix.get(pos);
	}

	/** Sets the value of a matrix element.
	 *
	 * @param pos position in the list.
 	 * @param value new value.
	 *
	 * @see SciMatrix#setValue(int,int,double) setValue(int row, int column, double value)
	 */
	public void setValue(int pos, double value)
	{
		this.matrix.set(pos,value);
	}

	/** Converts a string containing a matrix representation. Columns must
	 * be separated by a comma and rows by a semicolon. Moreover the matrix
	 * is written row by row.
	 * Example : 1,2;3,4
	 *
	 * @param txtmat string containing the matrix representation.
	 *
	 * @throws SciMatrixException if an error occured during the parse operation.
	 */
	public void toMatrix(String txtmat) throws SciMatrixException
	{
		String ntxtmat = "";
		int l = txtmat.length();
		int rows = 0;
		int columns = 0;
		this.matrix.clear();
		// Supression des espaces et récupération des dimensions.
		if (l > 0)
		{
			rows = 0;
			columns = 0;
			for (int i = 0; i < l; i++)
			{
				// On ignore les caractères spéciaux et les espaces.
				if(txtmat.charAt(i) > 32)
				{
					// On prend la prochaine valeur
					ntxtmat += txtmat.charAt(i);

					// On compte les lignes et le colonnes avec une gestion des erreurs (incohérence).
					if(txtmat.charAt(i) == ',')	// Séparateur de colonnes
					{
						columns++;
					}
					else if ((txtmat.charAt(i) == ';')||(i == (l-1)))	// Séparateur de lignes
					{
						columns++;
						if(rows == 0)
						{
							this.columns = columns;
						}
						else
						{
							if(columns != this.columns)
							{
								// Erreur, 2 lignes n'ont pas la même longueur.
								throw new SciMatrixException(SciMatrixException.E_ILL_ROWS,ntxtmat);
							}
						}
						rows++;
						columns = 0;
					}
				}
			}
			this.rows = rows++;
		}
		else
		{
			// Erreur : il n'y a pas de texte
			throw new SciMatrixException(SciMatrixException.E_NOTXT, "");
		}
		// Récupération des éléments
		String sval = "";
		l = ntxtmat.length();
		for(int i = 0; i < l; i++)
		{
			// On analyse chaque élément.
			if( (ntxtmat.charAt(i) == ',') || (ntxtmat.charAt(i) == ';') )
			{
				try
				{
					this.add(Double.parseDouble(sval));
				}
				catch(NumberFormatException e)
				{
					throw new SciMatrixException(SciMatrixException.E_PARSE_VAL, sval,e);
				}
				sval = "";
			}
			else
			{
				sval  += ntxtmat.charAt(i);
			}
		}
		try
		{
			this.add(Double.parseDouble(sval));
		}
		catch(NumberFormatException e)
		{
			throw new SciMatrixException(SciMatrixException.E_PARSE_VAL, sval,e);
		}
	}
}



