/*	SciMatrixException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Gestion des exceptions pour la conversion de chaine en matrice et
 * inversement.
 *
 */

package SciMatrix;

public class SciMatrixException extends Exception
{
	/** unknow error. */
	public final static int E_UNKNOW = 0;
	/** [size error] two rows or more have different sizes. */
	public final static int E_ILL_ROWS = 1;
	/** [parse error] no text to parse. */
	public final static int E_NOTXT = 2;
	/** [parse error] Incorrect number format */
	public final static int E_PARSE_VAL = 3;


	private int code;
	private String msg;

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 */
	public SciMatrixException(int code, String msg)
	{
		super();
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception without detail message.
	 *
	 */
	public SciMatrixException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public SciMatrixException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public SciMatrixException(Throwable cause)
	{
		this(0,"",cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case SciMatrixException.E_ILL_ROWS:
				return "[size error] two rows or more have different sizes";
			case SciMatrixException.E_NOTXT:
				return "[parse error] no text to parse";
			case SciMatrixException.E_PARSE_VAL:
				return "[parse error] Incorrect number format";
		}
		return "unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error (SciMatrix) " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (" + this.msg + ").";

		return tmp;
	}

}

