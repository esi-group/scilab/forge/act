/*	SysEquLinException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Gestion des erreurs dans l'écriture des système d'équation différentiel
 * linéaire.
 */

package SysEquLin;

public class SysEquLinException extends Exception
{
	/** unknow error. */
	public final static int E_UNKNOW = 0;
	/** [SysEquLin] the value is not correct */
	public final static int E_PARSE_D = 1;
	/** [SysEquLin] Element expected before '*' symbol */
	public final static int E_PARSE_VAL = 2;
	/** [SysEquLin] Element expected after '*' symbol */
	public final static int E_PARSE_STT = 3;

	/** [SysEquLin] Differential equation or output expected */
	public final static int E_TYPE_EQU  = 4;
	/** [SysEquLin] Parse error with the output ID */
	public final static int E_YPARSE_ID = 5;
	/** [SysEquLin] Positive ID needed */
	public final static int E_ID	    = 6;

	public int code;
	public String msg;

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 */
	public SysEquLinException(int code, String msg)
	{
		super();
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception without detail message.
	 *
	 */
	public SysEquLinException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public SysEquLinException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public SysEquLinException(Throwable cause)
	{
		this(0,"",cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case SysEquLinException.E_PARSE_D:
				return "[SysEquLin] the value is not correct";
			case SysEquLinException.E_PARSE_VAL:
				return "[SysEquLin] Element expected before '*' symbol";
			case SysEquLinException.E_PARSE_STT:
				return "[SysEquLin] Element expected after '*' symbol";
			case SysEquLinException.E_TYPE_EQU:
				return "[SysEquLin] Differential equation or output expected";
			case SysEquLinException.E_YPARSE_ID:
				return "[SysEquLin] Parse error with the output ID";
			case SysEquLinException.E_ID:
				return "[SysEquLin] Positive ID needed";
		}
		return "Unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error (SysEquLin) " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (" + this.msg + ").";

		return tmp;
	}




}










