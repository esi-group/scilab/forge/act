/*	EquLinNome.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Basis element for linear differential equation : <coef> * <state>
 */

package SysEquLin;

/** Basis element for linear differential equation : <coef> * <state>
 */
public class EquLinNome
{
	/** Coefficient. */
	private double a;
	/** state id (number) */
	private int state;
	/** type of element (+1 : state, -1 : input) */
	private int type;

	/** Initializes a newly created EquLinNome object with user defined
	 * state (by value), coefficient and type.
	 *
	 * @param state state id (state number).
	 * @param a coefficient.
	 * @param type type of element (state of input).
	 */
	public EquLinNome(int state, double a, int type)
	{
		this.a = a;
		this.state = state;
		this.type = type;
	}

	/** Initializes a newly created EquLinNome object with user defined
	 * state (by name), coefficient and type.
	 *
	 * @param name state name.
	 * @param a coefficient.
	 * @param slist states list.
	 * @param ulist inputs list.
	 */
	public EquLinNome(String name, double a,StateList slist, StateList ulist)
	{
		this.a = a;
		if(ulist != null)
		{
			if(ulist.exist(name))
			{
				this.type = -1;
				this.state = ulist.getState(name);
			}
			else
			{
				this.type = +1;
				this.state = slist.getState(name);
			}
		}
		else
		{
			this.type = +1;
			this.state = slist.getState(name);
		}
	}

	/* Parses a "nome" containing a string representation of a EquLinNome
	 * to find the state (by name) and the coefficient.
	 *
	 *	Warning : The sign (+ or -) must be in the string object (at
	 * the beginning).
	 *
	 * @param nome string to be parsed.
	 * @param slist states list.
	 * @param ulist inputs list.
	 *
	 * @throws SysEquLinException if a parse error occur.
	 */
	public EquLinNome(String nome, StateList slist, StateList ulist) throws SysEquLinException
	{
		String value = "";
		String state = "";
		int l = nome.length();
		int i = 0;
		boolean mul = false;

		// On suppose qu'il y a une valeur avant un '*' que l'on récupère.
		while((nome.charAt(i) != '*') && (i < l))
		{
			value += nome.charAt(i);
			i++;
			if(i >= l) break;
		}
		if(i < l)
		{
			if (nome.charAt(i) == '*')
				mul = true;
			i++;	// on passe le caractère '*';
		}
		// On récupère jusqu'à la fin : le nom de l'état.
		while(i<l)
		{
			state += nome.charAt(i);
			i++;
		}
		value = value.trim();
		state = state.trim();

		if(mul == true)
		{
			if(value.length() <= 0)
			{
				throw new SysEquLinException(SysEquLinException.E_PARSE_VAL,nome);
			}
			if(state.length() <= 0)
			{
				throw new SysEquLinException(SysEquLinException.E_PARSE_STT,nome);
			}
		}

		// Si le première caractère de "value" est bien un nombre
		// on suppose qu'il s'agit d'un nombre : on l'analyse.
		char tmp = value.charAt(1);
		if ((tmp >= '0') && (tmp <= '9'))
		{
			try
			{
				this.a = Double.parseDouble(value);
			}
			catch(NumberFormatException e)
			{
				throw new SysEquLinException(SysEquLinException.E_PARSE_D, value, e);
			}
		}
		
		// Si il y a bien un état (ou une entrée)
		if (state.length() > 0)
		{
			if (ulist.exist(state))
			{
				this.state = ulist.getState(state);
				this.type = -1;
			}
			else
			{
				this.state = slist.getState(state);
				this.type = +1;
			}
		}
		else
		{
			// Si on a une constante (pas d'état)
			if ( ((tmp >= '0') && (tmp <= '9')) )
			{
				this.state = 0;
				this.type = +1;
			}
			// Ou si on a pas de coefficient
			else
			{
				if(value.charAt(0) == '-')
					this.a = -1.0;
				else
					this.a = 1.0;
				value = value.substring(1);
				if(ulist.exist(value))
				{
					this.state = ulist.getState(value);
					this.type = -1;
				}
				else
				{
					this.state = slist.getState(value);
					this.type = +1;
				}
			}
		}
			
	}

	/** Creates a EquLinNome without importance.
	 */
	public EquLinNome()
	{
		this(-1,0.0,+1);
	}

	/** Gets the coefficient.
	 *
	 * @return coefficient.
	 */
	public double getA()
	{
		return this.a;
	}

	/** Gets the state id.
	 *
	 * @return state id.
	 */
	public int getState()
	{
		return this.state;
	}

	/** Gets element type.
	 *
	 * @return element type. 
	 */
	public int getType()
	{
		return this.type;
	}

	/** Sets element type.
	 *
	 * @param type new element type.
	 */
	public void setType(int type)
	{
		this.type = type;
	}

	/** Sets a new coefficient.
	 *
	 * @param a new coefficient.
	 */
	public void setA(double a)
	{
		this.a = a;
	}

	/** Sets state id.
	 *
	 * @param state state id.
	 */
	public void setState(int state)
	{
		this.state = state;
	}

	/** Stats state id by name.
	 * 
	 * @param list state list.
	 * @param name state name.
	 */
	public void setState(StateList list, String name)
	{
		this.state = list.getState(name);
	}

	/** Returns a string object representation of this EquLinNome object.
	 * States and inputs are represented by their number.
	 */
	public String toString()
	{
		String ret;
		if (this.a >= 0.0)
			ret = "+ " + this.a;
		else
			ret = "- " + (-this.a);

		if(this.state > 0)
		{
			if (this.type > 0)
				ret += " * <" + this.state + ">";
			else
				ret += " * <!" + this.state + "!>";
		}
		return ret;
	}

	/** Returns a string object representation of this EquLinNome object
	 * with the real state and input names.
	 *
	 * @param slist state list.
	 * @param ulist input list.
	 */
	public String toString(StateList slist, StateList ulist)
	{
		String ret;
		if (this.a >= 0.0)
			ret = "+ " + this.a;
		else
			ret = "- " + (-this.a);

		if(this.state > 0)
		{
			ret += " * ";
			if (this.type == 1)
				ret += slist.getName(this.state);
			else
				ret += ulist.getName(this.state);
		}
		return ret;
	}
}


