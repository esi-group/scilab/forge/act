import SysEquLin.StateList;
import SysEquLin.EquLin;

public class tstEquLin
{
	static public void main(String[] args)
	{
		StateList p = new StateList();
		System.out.println("x1 = " + p.getStateA("x1"));
		System.out.println("toto = " + p.getStateA("toto"));
		System.out.println("x2 = " + p.getStateA("x2"));
		System.out.println("toto = " + p.getStateA("toto"));
		System.out.println("x3 = " + p.getStateA("x3"));
		System.out.println("x4 = " + p.getStateA("x4"));
		System.out.println("x1 = " + p.getStateA("x1"));

		StateList i = new StateList();
		System.out.println("e = " + i.getStateA("e"));

		EquLin e = new EquLin(p,"x1");
		String myequ = " - 2.3 *   x1 + 2*x4 - 6*toto + 30 - x3 - 2 * x3 + e";
		System.out.println(myequ);
		e.setEquation(myequ,p,i);
		System.out.println(e);
		System.out.println(e.toString(p,i) + "\n\n");

		e.simplify();
		System.out.println(e.toString(p,i));

		System.out.println(e.toStringMatrixFormA());
		System.out.println(e.toStringMatrixFormB());
	}
}
