import SysEquLin.StateList;
import SysEquLin.StateListException;
import SysEquLin.EquLin;
import SysEquLin.SysEquLin;
import SysEquLin.SysEquLinException;

public class tstSysEquLin
{
	static public void main(String[] args)
	{
		try
		{
		StateList p = new StateList();
		System.out.println("x1 = " + p.getStateA("x1"));
		System.out.println("x2 = " + p.getStateA("x2"));

		StateList i = new StateList();
		System.out.println("e = " + i.getStateA("e"));

		SysEquLin s = new SysEquLin();
		String system = "dx2 := 2*x1  +   x2 -e ;\n dx1 := -    x1;\n";
		//system += "y1 := x1 + e;\n y2 := x2 - e;";
		s.setSystem(system,p,i);

		System.out.println("size : " + p.size());
		System.out.println("Nequs : " + s.getNequs());
		
		System.out.println(s);
		System.out.println(s.toString(p,i));

		System.out.println(s.toStringMatrixFormA(p));
		System.out.println(s.toStringMatrixFormB(i));

		System.out.print("\n");

		s.ordering();

		System.out.println(s.toString(p,i));

		System.out.println(s.toStringMatrixFormA(p));
		System.out.println(s.toStringMatrixFormB(i));
		System.out.println(s.toStringMatrixFormC(p));
		System.out.println(s.toStringMatrixFormD(i));
		}
		catch(SysEquLinException e)
		{
			e.printStackTrace();
		}
		catch(StateListException e)
		{
			e.printStackTrace();
		}
	}
}
