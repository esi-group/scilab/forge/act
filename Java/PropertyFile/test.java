import PropertyFile.PropertyFile;
import PropertyFile.PropertyFileException;

public class test
{
	static public void main(String[] args)
	{
		try
		{
			PropertyFile p = new PropertyFile("lang.i18n");
			System.out.println(p);

			PropertyFile s = new PropertyFile();
			s.add_nt("truc");
			s.add_nt("machin");
			s.setString("machin", "valeur1");
			double a = 25.2;
			s.setString("truc", ((Double)a).toString());
			s.save("write.txt");
		}
		catch(PropertyFileException e)
		{
			e.printStackTrace();
		}
	}
}
