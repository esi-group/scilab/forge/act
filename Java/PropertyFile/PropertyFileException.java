/*	PropertyFileException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Thrown to indicate a error when using a PropertyFile object or an language
 * file (i18n object).
 */

package PropertyFile;

/** Thrown to indicate a error when using a PropertyFile object or an language
 * file (i18n object).
 */
public class PropertyFileException extends Exception
{
	/** unknow erreur. */
	public final static int E_UNKNOW = 0;
	/** the file can't be opened. */
	public final static int E_OPENFILE = 1;
	/** a line can't be readed correctly. */
	public final static int E_READLINE = 2;
	/** the file can't be writed. */
	public final static int E_WRITE = 3;

	private int code;
	private String msg;

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message
	 */
	public PropertyFileException(int code, String msg)
	{
		super();
		this.msg = msg;
		this.code = code;
	}

	/** Constructs a new exception without code en detail message.
	 */
	public PropertyFileException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public PropertyFileException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.msg = msg;
		this.code = code;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public PropertyFileException(Throwable cause)
	{
		this(0,"",cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case PropertyFileException.E_OPENFILE:
				return "[PropertyFile] the file can't be opened";
			case PropertyFileException.E_READLINE:
				return "[PropertyFile] a line can't be readed correctly";
			case PropertyFileException.E_WRITE:
				return "[PropertyFile] the file can't be writed";
		}
		return "unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (" + this.msg + ").";

		return tmp;
	}
}






