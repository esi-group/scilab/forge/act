/*	i18n.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Permet de lire et écrire des fichiers de langues. Un fichier de langue
 * est un fichier de propriété (voir PropertyFile) ayant un nom particulier de
 * type :
 *	<base>_<langue>_<pays>.<extension>
 * ou,
 *	<base>_<langue>.<extension>
 * ou, en dernier choix,
 *	<base>.<extension>
 *
 *	On essaye d'ouvrir le fichier qui est le plus proche de la langue de
 * l'utilisateur (donc d'abord la variante national de la langue, puis la langue
 * et, si ces fichiers n'existe pas, la langue par défaut).
 */

package PropertyFile;

/** Allows to open the internationalization (i18n) file that is closest to the
 * user's language (first national variant, "official" language and then default
 * language).
 */	
public class i18n extends PropertyFile
{
	/** Defines the user locale language. */
	private java.util.Locale loc;
	/** Base name of the i18n file. */
	private java.lang.String fname;
	/** Extension of the i18n file. */
	private java.lang.String fext;
	/** Directory of the i18n file. */
	private java.lang.String dir;

	/** Initializes a newly created i18n object with user defined directory,
	 * locale language, base name and extension.
	 *
	 * @param dir directory of the i18n file.
	 * @param loc locale language.
	 * @param fname base name of the i18n file.
	 * @param fext extension of the i18n file.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public i18n(String dir,java.util.Locale loc, String fname, String fext) throws PropertyFileException
	{
		super();
		this.loc = loc;
		this.fname = fname;
		this.fext = fext;
		this.dir = dir;
		this.load();
	}

	/** Initializes a newly created i18n object with user locale language,
	 * base name and extension (the file must be in the current directory).
	 *
	 * @param loc locale language.
	 * @param fname base name of the i18n file.
	 * @param fext extension of the i18n file.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public i18n(java.util.Locale loc, String fname, String fext) throws PropertyFileException
	{
		this("",loc,fname,fext);
	}

	/** Initializes a newly created i18n object with user defined directory,
	 * locale language, base name (using the default extension "i18n").
	 *
	 * @param dir directory of the i18n file.
	 * @param loc locale language.
	 * @param fname base name of the i18n file.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public i18n(String dir,java.util.Locale loc, String fname) throws PropertyFileException
	{
		this(dir,loc,fname,"i18n");
	}

	/** Initializes a newly created i18n object with user defined locale language
	 * and base name (using the default extension "i18n" and the file must be
	 * in the current directory).
	 *
	 * @param loc locale language.
	 * @param fname base name of the i18n file.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public i18n(java.util.Locale loc, String fname) throws PropertyFileException
	{
		this(loc,fname,"i18n");
	}

	/** Initializes a newly created i18n object with user defined locale language
	 * and directory (using the default extension "i18n" and default base name "lang").
	 *
	 * @param dir directory of the i18n file.
	 * @param loc locale language.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public i18n(String dir, java.util.Locale loc) throws PropertyFileException
	{
		this(dir, loc,"lang");
	}

	/** Initializes a newly created i18n object with user defined locale language
	 * (using the default extension "i18n", default base name "lang" and
	 * the file must be in the current directory).
	 *
	 * @param loc locale language.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public i18n(java.util.Locale loc) throws PropertyFileException
	{
		this(loc,"lang");
	}
	
	/** Creates an empty i18n object.
	 */
	public i18n() throws PropertyFileException
	{
		this(null);
	}

	/** Sets the locale language.
	 *
	 * @param loc local language.
	 */
	public void setLocale(java.util.Locale loc)
	{
		this.loc = loc;
	}

	/** Gets the locale language.
	 *
	 * @return local language.
	 */
	public java.util.Locale getLocale()
	{
		return this.loc;
	}

	/** Sets the base name of the file.
	 *
	 * @param fname base name.
	 */
	public void setFileName(String fname)
	{
		this.fname = fname;
	}

	/** Gets the base name of the file.
	 *
	 * @return base name.
	 */
	public String getFileName()
	{
		return this.fname;
	}

	/** Sets the file extension.
	 *
	 * @param fext file extension.
	 */
	public void setFileExt(String fext)
	{
		this.fext = fext;
	}

	/** Gets the file extension.
	 *
	 * @return file extension.
	 */
	public String getFileExt()
	{
		return this.fext;
	}

	/** Sets the file directory.
	 *
	 * @param dir file directory.
	 */
	public void setDir(String dir)
	{
		this.dir = dir;
	}

	/** Gets the file directory.
	 *
	 * @return file directory.
	 */
	public String getDir()
	{
		return this.dir;
	}

	/** Loads an i18n file with parameters previously user defined.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public void load() throws PropertyFileException
	{
		java.io.File file = null;
		try
		{
			if (this.loc != null)	// Si la langue est définit.
			{
				String language = this.loc.getLanguage();
				String country = this.loc.getCountry();
				// On essaye de choisir la langue local.
				file = new java.io.File(this.dir + this.fname + "_" + language + "_" + country + "." + this.fext);
				if (!file.exists())
				{
					// Puis la langue général.
					file = new java.io.File(this.dir + this.fname + "_" + language + "." + this.fext);
					if (!file.exists())
					{
						// Puis la langue par défaut
						file = new java.io.File(this.dir + this.fname + "." + this.fext);
					}
				}
			}
			else	// Si la langue n'est pas définit :
			{
				// On charge la langue par défaut.
				file = new java.io.File(this.dir + this.fname + "." + this.fext);
			}
			super.load(file);			
		}
		catch(NullPointerException e)
		{
			throw new PropertyFileException(PropertyFileException.E_OPENFILE, file.toString(), e);
		}
		catch (SecurityException e)
		{
			throw new PropertyFileException(PropertyFileException.E_UNKNOW, file.toString(), e);
		}
	}


}
