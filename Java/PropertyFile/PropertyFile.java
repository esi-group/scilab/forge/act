/*	PropertyFile.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Allows to read and write property files. This files contain a list of
 * values associate with a name like :
 *	<name1> = <value1>
 *	<name2> = <value2>
 *		...
 *	<nameN> = <valueN>
 *
 *	Value is optional (without value it must not have '=' symbol).
 *	A line can start by '#' and it means that the line is a comment.
 */

package PropertyFile;

/** Allows to read and write property files. This files contain a list of
 * values associate with a name like :
 *	<name1> = <value1>
 *	<name2> = <value2>
 *		...
 *	<nameN> = <valueN>
 *
 *	Value is optional (without value it must not have '=' symbol).
 *	A line can start by '#' and it means that the line is a comment.
 */
public class PropertyFile
{
	/** List of property elements.*/
	private java.util.ArrayList<PropertyElement> plist;
	
	/** Initializes a newly created PropertyFile object with an empty list.
	 */
	public PropertyFile()
	{
		this.plist = new java.util.ArrayList<PropertyElement>();
	}

	/** Initializes a newly created PropertyFile object with a file.
	 *
	 * @param pfile file object containing the file to be read.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public PropertyFile(java.io.File pfile) throws PropertyFileException
	{
		this();
		this.load(pfile);
	}

	/** Initializes a newly created PropertyFile object with a file.
	 *
	 * @param pfile string object containing the file path of the file to be read.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public PropertyFile(String pfile) throws PropertyFileException
	{
		this(new java.io.File(pfile));
	}

	/** Loads a property file (creates the elements list)
	 *
	 * @param pfile file object containing the file to be read.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public void load(java.io.File pfile) throws PropertyFileException
	{
		this.plist.clear();
		java.io.BufferedReader br = null;
		String line = "";
		String[] sline;
		String sline0 = "";
		String sline1 = "";

		try
		{
			br = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(pfile),"UTF-8"));
		}
		catch (java.io.FileNotFoundException e)
		{
			throw new PropertyFileException(PropertyFileException.E_OPENFILE, pfile.toString(), e);
		}
		catch (SecurityException e)
		{
			throw new PropertyFileException(PropertyFileException.E_UNKNOW, pfile.toString(), e);
		}
		catch (java.io.UnsupportedEncodingException e)
		{
			throw new PropertyFileException(PropertyFileException.E_OPENFILE, pfile.toString(), e);
		}
	
		// Ouverture du fichier :
		try
		{
			// Lecture du fichier ligne par ligne :
			while( (line = br.readLine()) != null)
			{
				if(line.length() > 0)	// Si la ligne n'est pas vide
				{
					if(line.indexOf("=") >= 0)	// Si il y a un égale dans sur la ligne
					{
						// On récupère le nom et la valeur
						sline0 = line.substring(0,line.indexOf("="));
						sline1 = line.substring(line.indexOf("=")+1);
						// et on les ajoutes à la liste.
						this.plist.add(new PropertyElement(sline0.trim(),sline1.trim()));
					}
					else
					{
						// Ici, on a soit un commentaire seul, soit un nom (sans valeur) suivi d'un commentaire.
						sline = line.split("#"); // À MODIF (pour éviter le split qui peut créer plus de 2 chaines).
						sline[0] = sline[0].trim();
						if (sline[0].length() > 0) // Si on a un nom (sans valeur) on l'ajoute à la liste.
							this.plist.add(new PropertyElement(sline[0]));
					}
				}
			}
			
		}
		catch(java.io.IOException e)
		{
			throw new PropertyFileException(PropertyFileException.E_READLINE, line, e);
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(java.io.IOException e)
			{
				throw new PropertyFileException(PropertyFileException.E_UNKNOW,"",e);
			}
		}
	}

	/** Loads a property file (creates the elements list)
	 *
	 * @param pfile string object containing the file path of the file to be read.
	 *
	 * @throws PropertyFileException if an error occur during the reading.
	 */
	public void load(String pfile) throws PropertyFileException
	{
		try
		{
			this.load(new java.io.File(pfile));
		}
		catch(NullPointerException e)
		{
			throw new PropertyFileException(PropertyFileException.E_OPENFILE, pfile, e);
		}
	}

	/* Saves the PropertyFile object in a file (without comment : raw file).
	 *
	 * @param pfile file object containing the file to be read.
	 *
	 * @throws PropertyFileException if an error occur during the writing.
	 */
	public void save(java.io.File pfile) throws PropertyFileException
	{
		java.io.BufferedWriter bw;
		String tmp = "";
		try
		{
			bw = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(pfile),"UTF-8"));
			for (PropertyElement e : this.plist)
			{
				tmp = e.getName();
				if (e.getValue() != null)
					tmp += " = " + e.getValue();
				bw.write(tmp);
				tmp = "";
				bw.newLine();
			}
			bw.close();
		}
		catch (java.io.FileNotFoundException e)
		{
			throw new PropertyFileException(PropertyFileException.E_OPENFILE, pfile.toString(), e);
		}
		catch (SecurityException e)
		{
			throw new PropertyFileException(PropertyFileException.E_UNKNOW, pfile.toString(), e);
		}
		catch(java.io.IOException e)
		{
			throw new PropertyFileException(PropertyFileException.E_WRITE, pfile.toString(), e);
		}
	}

	/* Saves the PropertyFile object in a file (without comment : raw file).
	 *
	 * @param pfile string object containing the file path of the file to be read.
	 *
	 * @throws PropertyFileException if an error occur during the writing.
	 */
	public void save(String pfile) throws PropertyFileException
	{
		try
		{
			this.save(new java.io.File(pfile));
		}
		catch(NullPointerException e)
		{
			throw new PropertyFileException(PropertyFileException.E_OPENFILE, pfile, e);
		}
	}
	
	/** Allows to know if a given name already exist in the list or not.
	 *
	 * @param name element name.
	 *
	 * @return true if the name exist and else false.
	 */
	public boolean exist(String name)
	{
		for(PropertyElement e : this.plist)
		{
			if(e.getName().equals(name))
			{
				return true;
			}
		}
		return false;
	}

	/** Sets the value associated to a name.
	 *
	 * @param name element name.
	 * @param value new element value.
	 */
	public void setString(String name, String value)
	{
		for(PropertyElement e : this.plist)
		{
			if(e.getName().equals(name))
			{
				e.setValue(value);
			}
		}
	}

	/** Adds a property but without test to know if the name already exist
	 * (it suppose no).
	 *
	 * @param name property name.
	 * @param val property value.
	 */
	public void add_nt(String name, String val)
	{
		this.plist.add(new PropertyElement(name, val));
	}

	/** Adds a property without value and without test to know if the name
	 * already exist (it suppose no)
	 *
	 * @param name property name.
	 *
	 * @see PropertyFile#add_nt(String,String) add_nt(String name, String val)
	 */
	public void add_nt(String name)
	{
		this.plist.add(new PropertyElement(name, ""));
	}

	/** Adds property to the list with test to know if the name already exist.
	 *
	 * @param name property name.
	 * @param val property value.
	 */
	public void add(String name, String val)
	{
		if(!this.exist(name))
		{
			this.add_nt(name, val);
		}
	}

	/** Adds property without value but with test to know if the name already exist.
	 *
	 * @param name property name.
	 *
	 * @see PropertyFile#add(String,String) add(String name, String val)
	 */
	public void add(String name)
	{
		this.add(name, "");
	}

	/* Removes all the properties
	 *
	 */
	public void removeAll()
	{
		this.plist.clear();
	}

	/** Gets the values associated to a name (returns 'null' if the name
	 * does not exist).
	 *
	 * @param name property name.
	 *
	 * @return property value.
	 */
	public String getString(String name)
	{
		for(PropertyElement e : this.plist)
		{
			if(e.getName().equals(name))
			{
				return e.getValue();
			}
		}
		return null;
	}

	/** Gets the value associated to a name (returns 'null' if the name
	 * does not exist).
	 *	Here '#<n>' symbols (where '<n>' is a number) are replace by
	 * the value contained in value list (converted into character).
	 *
	 * @param name property name.
	 * @param var values list.
	 *
	 * @return modified property value.
	 */
	public String getString(String name, java.util.ArrayList<String> var)
	{
		String ret = this.getString(name);
		if (ret != null)
		{
			for(int i = 0; i < var.size(); i++)
			{
				ret = ret.replaceAll("#"+(i+1), var.get(i));
			}
		}
		return ret;
	}

	/** Gets the value associated to a name (returns 'null' if the name
	 * does not exist).
	 *	Here '#1' symbols are replace by the value contained in value
	 * list (converted into character).
	 *
	 * @param name property name.
	 * @param var value.
	 *
	 * @return modified property value.
	 */
	public String getString(String name, String var)
	{
		java.util.ArrayList<String> tmp = new java.util.ArrayList<String>();
		tmp.add(var);
		return this.getString(name,tmp);
	}

	/** Gets the value associated to a name (returns 'null' if the name
	 * does not exist).
	 *	Here '#1' and '#2' symbols are replace by the value contained in 
	 * value list (converted into character).
	 *
	 * @param name property name.
	 * @param var1 value associated to '#1'.
	 * @param var2 value associated to '#2'.
	 *
	 * @return modified property value.
	 */
	public String getString(String name, String var1, String var2)
	{
		java.util.ArrayList<String> tmp = new java.util.ArrayList<String>();
		tmp.add(var1);
		tmp.add(var2);
		return this.getString(name,tmp);
	}

	/** Returns a string object representing this PropertyFile.
	 *
	 * @return a string representation of the PropertyFile.
	 */
	public String toString()
	{
		String ret = "";
		for(PropertyElement e : this.plist)
		{
			ret += e.getName();
			if(e.getValue() != null)
				ret += " = " + e.getValue();
			ret += "\n";
		}
		return ret;
	}
	
}
