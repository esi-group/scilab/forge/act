/*	CloudDataException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Gestion des erreurs dans la gestion des données.
 */

package CloudData;

import PropertyFile.i18n;

public class CloudDataException extends Exception
{
	/** unknow error */
	public static final int E_UNKNOW = 0;

	public static final int E_MAT_A_UNKNOW = 100;	// E0100 : La matrice A n'est pas correct.
	public static final int E_MAT_A_NOSQRT = 101;	// E0101 : La matrice A n'est pas carré.
	public static final int E_MAT_A_DIMERR = 102;	// E0102 : Dimension de A incohérente.
	public static final int E_MAT_A_EMPTY  = 103;	// E0103 : A est vide.
	public static final int E_MAT_A_PARSE  = 104;	// E0104 : Une des valeurs de A est mal écrite.

	public static final int E_MAT_B_UNKNOW = 110;	// E0110 : La matrice B n'est pas correct.
	public static final int E_MAT_B_DIFORD = 111;	// E0111 : Le nombre de lignes de B est différent de l'ordre du système.
	public static final int E_MAT_B_DIMERR = 112;	// E0112 : Dimension de B incohérente.
	public static final int E_MAT_B_EMPTY  = 113;	// E0113 : B est vide.
	public static final int E_MAT_B_PARSE  = 114;	// E0114 : Une des valeurs de B est mal écrite.

	public static final int E_MAT_C_UNKNOW = 120;	// E0120 : La matrice C n'est pas correct.
	public static final int E_MAT_C_DIFORD = 121;	// E0121 : Le nombre de colonnes de C est différent de l'ordre du système.
	public static final int E_MAT_C_DIMERR = 122;	// E0122 : Dimension de C incohérente.
	public static final int E_MAT_C_EMPTY  = 123;	// E0123 : C est vide.
	public static final int E_MAT_C_PARSE  = 124;	// E0124 : Une des valeurs de C est mal écrite.

	public static final int E_MAT_K_UNKNOW = 130;	// E0130 : La matrice K n'est pas correct.
	public static final int E_MAT_K_DIFORD = 131;	// E0131 : Le nombre de colonnes de K est différent de l'ordre du système.
	public static final int E_MAT_K_DIMERR = 132;	// E0132 : Dimension de K incohérente.
	public static final int E_MAT_K_EMPTY  = 133;	// E0133 : K est vide.
	public static final int E_MAT_K_PARSE  = 134;	// E0134 : Une des valeurs de K est mal écrite.

	public static final int E_H_UNKNOW     = 200; 	// E0200 : Erreur inconnu dans la fonction de transfert.
	public static final int E_H_K_PARSE    = 201;	// E0201 : Erreur sur la lecture du gain.
	public static final int E_H_N_PARSE    = 211;	// E0211 : Erreur lors de l'analyse d'une valeur d'un monôme du numérateur.
	public static final int E_H_N_POWNEED  = 212; 	// E0212 : Le symbole '^' est attendu après une des variables du numérateur.
	public static final int E_H_N_BRACKET  = 213;	// E0213 : Il manque une parenthèse aux numérateur.
	public static final int E_H_N_EMPTYD   = 214;	// E0214 : Le numérateur est vide.
	public static final int E_H_N_EMPTYP   = 221;	// E0221 : La chaine du numérateur est vide.
	public static final int E_H_N_EMPTY    = 222;	// E0222 : La liste des monômes du numérateur est vide.
	public static final int E_H_N_SIGN     = 223;	// E0223 : Le numérateur ne doit pas avoir des puissances positives et négatives en même temps.
	public static final int E_H_D_PARSE    = 231;	// E0231 : Erreur lors de l'analyse d'une valeur d'un monôme du dénominateur.
	public static final int E_H_D_POWNEED  = 232;	// E0232 : Le symbole '^' est attendu après une des variables du dénominateur.
	public static final int E_H_D_BRACKET  = 233;	// E0233 : Il manque une parenthèse aux dénominateur.
	public static final int E_H_D_EMPTYD   = 234;	// E0234 : Le dénominateur est vide.
	public static final int E_H_D_EMPTYP   = 241;	// E0241 : La chaine du dénominateur est vide.
	public static final int E_H_D_EMPTY    = 242;	// E0242 : La liste des monômes du dénominateur est vide.
	public static final int E_H_D_SIGN     = 243;	// E0243 : Le dénominateur ne doit pas avoir des puissances positives et négatives en même temps.

	public static final int E_POLES_UNKNOW = 300;	// E0300 : Erreur inconnu dans la liste des pôles.
	public static final int E_POLES_PARSE  = 301;	// E0301 : Un pôle au moins est mal écrit.

	public static final int E_ALPHA_UNKNOW = 400;	// E0400 : Erreur inconnu dans la liste des alpha.
	public static final int E_ALPHA_PARSE  = 401;   // E0401 : Un alpha au moins est mal écrit.

	public static final int E_EQL_S_UNKNOW = 500;	// E0500 : Erreur inconnu sur la liste d'états.
	public static final int E_EQL_S_DIGIT  = 501;	// E0501 : Le nom d'un état ne doit pas commencé par un chiffre.

	public static final int E_EQL_I_UNKNOW = 510;	// E0510 : Erreur inconnu sur la liste de sorties.
	public static final int E_EQL_I_DIGIT  = 511;	// E0511 : Le nom d'une sortie ne doit pas commencé par un chiffre.

	public static final int E_EQU_UNKNOW   = 520;	// E0520 : Erreur inconnu sur le système.
	public static final int E_EQU_PARSE    = 521;	// E0521 : Erreur sur la lecture d'une valeur.
	public static final int E_EQU_PARSE_V  = 522;	// E0522 : Un élément est attendu avant le symbole '*'.
	public static final int E_EQU_PARSE_S  = 523;	// E0523 : Un élément est attendu après le symbole '*'.
	public static final int E_EQU_NEQUS    = 524;	// E0522 : Il manque des équations.

	public static final int E_S_ORDER      = 1010;  // E1010 : Le nombre de pôles est différent de l'ordre du système.
	public static final int E_S_SCILAB     = 1020;	// E1020 : Erreur dans l'exécution du script Scilab.

	public static final int E_S_PP_UNKNOW  = 1100;	// E1100 : Erreur inconnu dans la liste des pôles. (Placement de pôles)
	public static final int E_S_PP_PARSE   = 1101;	// E1101 : Un pôle au moins est mal écrit. (Placement de pôles)

	public static final int E_S_AC_UNKNOW  = 1200;	// E1200 : Erreur inconnu dans la liste des pôles. (Ackerman)
	public static final int E_S_AC_PARSE   = 1201;	// E1201 : Un pôle au moins est mal écrit. (Ackerman)

	public static final int E_S_BR_UNKNOW  = 1300;	// E1300 : Erreur inconnu dans la liste des pôles. (Brunovsky)
	public static final int E_S_BR_PARSE   = 1301;	// E1301 : Un pôle au moins est mal écrit. (Brunovsky)

	public static final int E_P_STD_LUNK   = 2100;	// E2100 : Erreur inconnu dans la liste des équations.
	public static final int E_P_STD_LEXP   = 2101;	// E2101 : Un nombre est attendu avec le symbole '*'.
	public static final int E_P_STD_LFMT   = 2102;	// E2102 : Le format du nombre est incorrect.
	public static final int E_P_STD_EUNK   = 2110;	// E2110 : Erreur inconnu dans la liste des équations.
	public static final int E_P_STD_EYOUT  = 2111;	// E2111 : La variable de sortie doit être 'y'.
	public static final int E_P_STD_EPAR   = 2112;	// E2112 : Erreur dans l'analyse de l'identifiant de sortie.
	public static final int E_P_STD_ECONF  = 2120;	// E2120 : Deux équations ont le même identifiant de sortie.

	public static final int E_S_SIM_UNKNOW = 3100;	// E3100 = Erreur inconnu sur la simulation de la stabilisation.
	public static final int E_S_SIM_NSTAB  = 3101;	// E3101 = Le système doit être stabilisé.
	public static final int E_S_SIM_TI     = 3110;	// E3110 = Erreur dans l'analyse du temps initial.
	public static final int E_S_SIM_TF     = 3111;  // E3111 = Erreur dans l'analyse du temps final.
	public static final int E_S_SIM_STEP   = 3112;	// E3112 = Erreur dans l'analyse du pas.
	public static final int E_S_SIM_PARAM  = 3113;	// E3113 = Erreur dans le paramètre.
	public static final int E_S_SIM_METHOD = 3114;	// E3114 = Méthode (fonction) inexistante.
	public static final int E_S_SIM_TYPEO  = 3120;	// E3120 = Type de sortie incorrect.
	public static final int E_S_SIM_NOUT   = 3121;	// E3121 = Erreur sur le numéro de la sortie.

	private int code;
	private String msg;
	private i18n lang;

	/** Constructs a new exception with the specified code, detail message,
	 * and i18n file.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param lang i18n file.
	 */
	public CloudDataException(int code, String msg, i18n lang)
	{
		super();
		this.code = code;
		this.msg = msg;
		this.lang = lang;
	}

	/** Constructs a new exception with specified code and i18n file.
	 *
	 * @param code error code.
	 * @param lang i18n file.
	 */
	public CloudDataException(int code, i18n lang)
	{
		this(code, "", lang);
	}

	/** Constructs a new exception with specified i18n file.
	 *
	 * @param lang i18n file.
 	 */
	public CloudDataException(i18n lang)
	{
		this(0,"", lang);
	}

	/** Constructs a new exception with the specified code, detail message,
	 * i18n file and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param lang i18n file.
	 * @param cause error cause.
	 */
	public CloudDataException(int code, String msg, i18n lang, Throwable cause)
	{
		super(cause);
		this.code = code;
		this.msg = msg;
		this.lang = lang;
	}

	/** Constructs a new exception with specified code, i18n file and cause.
	 *
	 * @param code error code.
	 * @param lang i18n file.
	 * @param cause error cause.
	 */
	public CloudDataException(int code, i18n lang, Throwable cause)
	{
		this(code, "", lang, cause);
	}

	/** Constructs a new exception with specified i18n file and cause.
	 *
	 * @param lang i18n file.
	 * @param cause error cause.
 	 */
	public CloudDataException(i18n lang, Throwable cause)
	{
		this(0,"",lang,cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode(int code)
	{
		String tmp = "E";
		if (code < 1000)
			tmp += "0";
		if (code < 100)
			tmp += "0";
		if (code < 10)
			tmp += "0";
		tmp += code;
		return this.lang.getString(tmp);
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (" + this.msg + ").";

		return tmp;
	}
	


}
















