/*	CloudData.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Contient toutes les données relatives au projet.
 */

package CloudData;

import PropertyFile.i18n;
import PropertyFile.PropertyFile;
import PropertyFile.PropertyFileException;

import java.util.Locale;

import Complex.ListComplex;
import Complex.ComplexException;

import SciMatrix.SciMatrix;
import SciMatrix.SciMatrixException;

import TransferFunction.TransferFunction;
import TransferFunction.TransferFunctionException;
import Polynomial.PolynomialException;

import SysEquLin.SysEquLin;
import SysEquLin.SysEquLinException;
import SysEquLin.StateList;
import SysEquLin.StateListException;

import PursuitEqus.PursuitEqus;
import PursuitEqus.ListLambda;
import PursuitEqus.PursuitEqusException;

import Search.Search;
import java.util.Locale;

import java.util.ArrayList;

/** Contains all project data.
 */
public class CloudData
{

// Constants : how is input the original system :

	/** Undefined system type. */
	public static final int CST_I_NULL = 0;
	/** Matrices (A, B and C). */
	public static final int CST_I_MAT = 1;
	/** System poles list.  */
	public static final int CST_I_POLE = 2;
	/** Coefficients list of the characteristic polynomial */
	public static final int CST_I_ALPHA = 3;
	/** Linear differential equations system */
	public static final int CST_I_EQU = 4;
	/** Transfer function */
	public static final int CST_I_H = 5;


// Constants : Stabilization :

	/** Undefined method */
	public static final int CST_S_NULL = 0;
	/** Poles placement */
	public static final int CST_S_PPOLES = 1;
	/** Ackerman method */
	public static final int CST_S_ACKER = 2;
	/** Brunovsky method */
	public static final int CST_S_BRUNOV = 3;


// Constants : Tracking (Pursuit)

	/** Undefined method */
	public static final int CST_P_NULL = 0;
	/** Our method */
	public static final int CST_P_DEFAULT = 1;

// Constants : Simulation (Stabilisation)

	/** Undefined input */
	public static final int CST_SIM_NULL = 0;
	/** Step function */
	public static final int CST_SIM_STEP = 1;
	/** Dirac function */
	public static final int CST_SIM_DIRAC = 2;
	/** Ramp function */
	public static final int CST_SIM_RAMP = 3;


// Variable : Methods :

	/** How is input the original system ? */
	private int systemInput = 0;
	/** What stabilization method is used ? */
	private int stabMethod = 0;
	/** What tracking (pursuit) method is used ? */
	private int pursuitMethod = 0;
	
// Other informations

	/** has the system been stabilized? */
	private boolean stabilized = false;
	/** has the system been tracked */
	private boolean tracked = false;

	/** does it uses the stabilized system for the tracking ? */
	private boolean useStab = true;

	/** System order (number of states) */
	private int sysorder = 0;
	/** Number of inputs */
	private int ninputs = 0;
	/** Number of outputs */
	private int noutputs = 0;


// Original system : Transfer function

	/** Transfer function object. */
	private TransferFunction H;
	/** Static gain (user string). */
	private String H_K = "1.0";
	/** Transfer function variable. */
	private char H_c = 's';
	/** Numerator (user string). */
	private String H_num = "";
	/** Denominator (user string). */
	private String H_den = "";

// Original system : Poles list

	/** Poles list object. */
	ListComplex poles = null;
	/** Poles list (user string). */
	String listpoles = "";

// Original system : Alpha list

	/** Alpha list object. */
	ListComplex alpha = null;
	/** Alpha list (user string) */
	String listalpha = "";

// Original system : Matrices

	/** A matrix */
	private SciMatrix matA = null;
	/** A matrix (user string) */
	private String tmatA = "";
	/** B matrix */
	private SciMatrix matB = null;
	/** B matrix (user string) */
	private String tmatB = "";
	/** C matrix */
	private SciMatrix matC = null;
	/** C matrix (user string) */
	private String tmatC = "";

// Original system : Linear differential equations

	/** States list */
	private StateList sysstates = null;
	/** States list (user string) */
	private String	tsysstates = "";
	/** Inputs list */
	private StateList sysinputs = null;
	/** Inputs list (user string) */
	private String tsysinputs = "";
	/** Linear system equations */
	private SysEquLin sys = null;
	/** Linear system equations (user string) */
	private String tsys = "";

// Stabilization : poles placement

	/** Poles list */
	ListComplex pp_poles = null;
	/** Poles list (user string) */
	private String tpp_poles = "";

// Stabilization : Ackerman

	/** Poles list */
	ListComplex acker_poles = null;
	/** Poles list (user string) */
	private String tacker_poles = "";
	
// Stabilization : Brunovsky

	/** Poles list */
	ListComplex bru_poles = null;
	/** Poles list (user string) */
	private String tbru_poles = "";

// Tracking (pursuit) : std

	/** Tracking equations (user string) */
	private String toequs;
	/** Tracking equations */
	private PursuitEqus oequs;
	/** Lambda lists (user string) */
	private String tolambda;
	/** Lambda lists */
	private ListLambda olambda;

// Stabilized system (matrices)
	private SciMatrix stabA = null;
	private SciMatrix stabB = null;
	private SciMatrix stabC = null;
	private SciMatrix stabK = null;
	

// Other

	/** i18n error file. */
	private i18n lang = null;

	/** Scilab directory. */
	private String pathScilab = "";
	/** Maxima directory. */
	private String pathMaxima = "";

	/** String representing the language. */
	private String tlang = "";
	/** String representing the country. */
	private String tcountry = "";
	/** i18n object. */
	private i18n TextLang = null;

	/** i18n files directory */
	private String diri18n = "";

	/** Had the project being already saved ? */
	private int isSaved = 0;


	/** Initialize a newly created CloudData object with a i18n error
	 * file object.
	 *
	 * @param lang i18n object containing error messages in the local
	 * 	language (or user defined language).
	 */
	public CloudData(i18n lang)
	{
		this.lang = lang;
	}	

	/** Initialize a newly created CloudData object without i18n error
	 * file object.
	 */
	public CloudData()
	{
		this(null);
	}

	/** Gets the i18n error file object.
	 *
	 * @return i18n object containing error messages in the local language
	 *	(or user defined language).
	 */
	public i18n getLang()
	{
		return this.lang;
	}

	/** Sets the i18n error file object.
	 *
	 * @param lang new i18n object containing error messages in the local
	 *	language (or user defined language).
	 */
	public void setLang(i18n lang)
	{
		this.lang = lang;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Sets the Scilab path.
	 *
	 * @param path Scilab path.
	 */
	public void setScilabPath(String path)
	{
		this.pathScilab = path;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Get the Scilab path.
	 *
	 * @return Scilab path.
	 */
	public String getScilabPath()
	{
		return this.pathScilab;
	}

	/** Sets the Maxima path.
	 *
	 * @param path Maxima path.
	 */
	public void setMaximaPath(String path)
	{
		this.pathMaxima = path;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the Maxima path.
	 *
	 * 	@return Maxima path.
	 */
	public String getMaximaPath()
	{
		return this.pathMaxima;
	}

	/** Gets the defined language.
	 *
	 *	@return string containing the language representation.
	 */
	public String getLanguage()
	{
		return this.tlang;
	}

	/** Gets the defined country.
	 *
	 *	@return string containing the country representation.
	 */
	public String getCountry()
	{
		return this.tcountry;
	}

	/** Detects automatically the scilab and maxima version and path.
	 *
	 */ 
	public void autodetect() throws PropertyFileException
	{
		Search s = new Search();
		this.pathScilab = s.getScilabPath();
		this.pathMaxima = s.getMaximaPath();
		
		Locale loc = Locale.getDefault();
		this.tlang = loc.getLanguage();
		this.tcountry = loc.getCountry();
		this.TextLang = new i18n(this.geti18nDir(), new Locale(this.tlang,this.tcountry));
		this.setLang(new i18n(this.geti18nDir(), new Locale(loc.getLanguage(),loc.getCountry()),"error"));
	}

	/** Loads the configuration file.
	 *
	 *	@throws PropertyFileException when a PropertyFile error occur.
	 */
	public void loadConfig() throws PropertyFileException, CloudDataException
	{
		String lang = "";
		String country = "";
		PropertyFile p;

		java.io.File pfile = new java.io.File("act.config");

		if (!pfile.exists())
		{
			this.autodetect();
			this.saveConfig();
		}
		else
		{
			try
			{
				p = new PropertyFile("act.config");
			}
			catch(PropertyFileException e)
			{
				throw new CloudDataException(10+e.getCode(),this.lang,e);
			}

			if (p.exist("scipath"))
			{
				this.pathScilab = p.getString("scipath");
			}

			if (p.exist("maxpath"))
			{
				this.pathMaxima = p.getString("maxpath");
			}

			if (p.exist("lang"));
			{
				lang = p.getString("lang");
			}

			if (p.exist("country"))
			{
				country = p.getString("country");
			}

			this.setTextLang(lang,country);
			this.setLang(new i18n(this.geti18nDir(), new Locale(lang,country),"error"));
		}
		return;
	}

	/** Saves the configuration file.
	 *
	 *	@throws CloudDataException when a PropertyFile error occur.
	 */
	public void saveConfig() throws CloudDataException
	{
		PropertyFile p = new PropertyFile();
		
		p.add_nt("scipath", this.pathScilab);
		p.add_nt("maxpath",  this.pathMaxima);
		p.add_nt("lang", this.tlang);
		p.add_nt("country", this.tcountry);

		try
		{
			p.save("act.config");
		}
		catch(PropertyFileException e)
		{
			throw new CloudDataException(10+e.getCode(),this.lang,e);
		}
		return;
	}

	/** Sets the language (for text)
	 *
	 * 	@param lang Language (FR, EN, DE, ...)
	 *	@param country Country (fr, gb, us, ...)
	 *
	 *	@throws PropertyFileException when a i18n error occur.
	 */
	public void setTextLang(String lang, String country) throws PropertyFileException
	{
		// TODO À tester !!!
		try
		{
			this.TextLang = new i18n(this.geti18nDir(), new Locale(lang, country));
		}
		catch(PropertyFileException e)
		{
			if (this.tlang != "")
			{
				String tmplang = this.tlang;
				String tmpcty = this.tcountry;
				this.tlang = "";
				this.tcountry = "";
				this.setTextLang(tmplang, tmpcty);
			}
			else
			{
				throw e;
			}
		}
		this.tlang = lang;
		this.tcountry = country;
	}

	/** get the language file (for text)
	 *
	 *	@return i18n object.
	 *	
	 *	@throws PropertyFileException when a i18n error occur.
	 */
	public i18n getTextLang() throws PropertyFileException
	{
		return this.TextLang;
	}

	/** Sets the i18n files directory.
	 *
	 * @param dir i18n files directory.
	 */
	public void seti18nDir(String dir)
	{
		this.diri18n = dir;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the i18n files directory.
	 *
	 * @return i18n files directory.
	 */
	public String geti18nDir()
	{
		return this.diri18n;
	}

	/** Sets the system order.
	 *
	 * @param sysorder new system order.
	 */
	public void setSysOrder(int sysorder)
	{
		this.sysorder = sysorder;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the system order.
	 *
	 * @return system order.
	 */
	public int getSysOrder()
	{
		return this.sysorder;
	}
	
	/** Sets the number of inputs.
	 *
	 * @param ninputs new number of inputs.
	 */
	public void setNinputs(int ninputs)
	{
		this.ninputs = ninputs;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the number of inputs.
	 *
	 * @return number of inputs.
	 */
	public int getNinputs()
	{
		return this.ninputs;
	}

	/** Sets the number of outputs.
	 *
	 * @param noutputs new number of outputs.
	 */
	public void setNoutputs(int noutputs)
	{
		this.noutputs = noutputs;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the number of outputs.
	 *
	 * @return number of outputs.
	 */
	public int getNoutputs()
	{
		return this.noutputs;
	}

	/** Returns true if the stabilization is done and else false.
	 *
	 * @return true if the stabilization is done and else false.
	 */
	public boolean isStabilized()
	{
		return this.stabilized;
	}

	/** Defines if the system is stabilized or not.
	 *
	 * @param stabilized true if the system is stabilized and else false.
	 */
	public void setStabilized(boolean stabilized)
	{
		this.stabilized = stabilized;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Returns true if the tracking is done and else false.
	 *
	 * @return true if the tracking is done and else false.
	 */
	public boolean isTracked()
	{
		return this.tracked;
	}

	/** Defines if the system is tracked or not.
	 *
	 * @param tracked stabilized true if the system is tracked and else false.
	 */
	public void setTacked(boolean tracked)
	{
		this.tracked = tracked;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}
	
	/** Saves A, B and C matrices input by the user without control and
	 * validation.
	 *
	 * @param A A matrix
	 * @param B B matrix
	 * @param C C matrix
	 */
	public void saveInputMatrix(String A, String B, String C)
	{
		this.tmatA = A;
		this.tmatB = B;
		this.tmatC = C;
	}

	/** Saves A, B and C matrices input by the user with control and
	 * validation (it verifies that they are correctly written and
	 * have correct dimensions).
	 *
	 * @param A A matrix
	 * @param B B matrix
	 * @param C C matrix
	 *
	 * @throws CloudDataException if a matrix validation error occur. 
	 */
	public void setInputMatrix(String A, String B, String C) throws CloudDataException
	{
		int n;	// Ordre du système.

		// On traite la matrice A :
		try
		{
			this.matA = new SciMatrix(A);
		}
		catch(SciMatrixException e)
		{
			this.matA = null;
			this.matB = null;
			this.matC = null;
			switch(e.getCode())
			{
				case SciMatrixException.E_ILL_ROWS:
					// Dimension de A incohérente.
					throw new CloudDataException(CloudDataException.E_MAT_A_DIMERR, this.lang,e);
				case SciMatrixException.E_NOTXT:
					// A est vide.
					throw new CloudDataException(CloudDataException.E_MAT_A_EMPTY, this.lang,e);
				case SciMatrixException.E_PARSE_VAL:
					// Une des valeurs de A est mal écrite.
					throw new CloudDataException(CloudDataException.E_MAT_A_PARSE, this.lang,e);
			}
			// La matrice A n'est pas correct.
			throw new CloudDataException(CloudDataException.E_MAT_A_UNKNOW,this.lang,e);
		}

		// On traite la matrice B
		try
		{
			this.matB = new SciMatrix(B);
		}
		catch(SciMatrixException e)
		{
			this.matA = null;
			this.matB = null;
			this.matC = null;
			switch(e.getCode())
			{
				case SciMatrixException.E_ILL_ROWS:
					// Dimension de B incohérente.
					throw new CloudDataException(CloudDataException.E_MAT_B_DIMERR, this.lang,e);
				case SciMatrixException.E_NOTXT:
					// B est vide.
					throw new CloudDataException(CloudDataException.E_MAT_B_EMPTY, this.lang,e);
				case SciMatrixException.E_PARSE_VAL:
					// Une des valeurs de B est mal écrite.
					throw new CloudDataException(CloudDataException.E_MAT_B_PARSE, this.lang,e);
			}
			// La matrice B n'est pas correct.
			throw new CloudDataException(CloudDataException.E_MAT_B_UNKNOW,this.lang,e);
		}

		// On traite la matrice C
		try
		{
			this.matC = new SciMatrix(C);
		}
		catch(SciMatrixException e)
		{
			this.matA = null;
			this.matB = null;
			this.matC = null;
			switch(e.getCode())
			{
				case SciMatrixException.E_ILL_ROWS:
					// Dimension de C incohérente.
					throw new CloudDataException(CloudDataException.E_MAT_C_DIMERR, this.lang,e);
				case SciMatrixException.E_NOTXT:
					// C est vide.
					throw new CloudDataException(CloudDataException.E_MAT_C_EMPTY, this.lang,e);
				case SciMatrixException.E_PARSE_VAL:
					// Une des valeurs de C est mal écrite.
					throw new CloudDataException(CloudDataException.E_MAT_C_PARSE, this.lang,e);
			}
			// La matrice C n'est pas correct.
			throw new CloudDataException(CloudDataException.E_MAT_C_UNKNOW,this.lang,e);
		}
		
		// On vérifie que la matrice A est bien carré (n*n).
		if ( (n = this.matA.getRows()) != this.matA.getColumns())
		{
			this.matA = null;
			this.matB = null;
			this.matC = null;
			// La matrice A n'est pas carré.
			throw new CloudDataException(CloudDataException.E_MAT_A_NOSQRT,this.lang);
		}

		// On vérifie que B a bien n lignes.
		if(n != this.matB.getRows())
		{
			this.matA = null;
			this.matB = null;
			this.matC = null;
			// Le nombre de lignes de B est différent de l'ordre du système.
			throw new CloudDataException(CloudDataException.E_MAT_B_DIFORD,this.lang);
		}

		// On vérifie que C a bien n colonnes.
		if(n != this.matC.getColumns())
		{
			this.matA = null;
			this.matB = null;
			this.matC = null;
			// Le nombre de colonnes de C est différent de l'ordre du système.
			throw new CloudDataException(CloudDataException.E_MAT_C_DIFORD,this.lang);
		}

		this.systemInput = CloudData.CST_I_MAT;

		this.tmatA = A;
		this.tmatB = B;
		this.tmatC = C;

		this.sysorder = n;
		this.ninputs = this.matB.getColumns();
		this.noutputs = this.matC.getRows();
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets a string object containing a representation of the A matrix.
	 *
	 * @return string object containing a representation of the A matrix.
	 */
	public String getMatrixA()
	{
		return this.matA.toString();
	}

	/** Gets string object containing the A matrix representation written	
	 * by the user.
	 *
	 * @return original string.
	 */
	public String getOriginalMatrixA()
	{
		return this.tmatA;
	}

	/** Gets a string object containing a representation of the B matrix.
	 *
	 * @return string object containing a representation of the B matrix.
	 */
	public String getMatrixB()
	{
		return this.matB.toString();
	}

	/** Gets string object containing the B matrix representation written	
	 * by the user.
	 *
	 * @return original string.
	 */
	public String getOriginalMatrixB()
	{
		return this.tmatB;
	}

	/** Gets a string object containing a representation of the C matrix.
	 *
	 * @return string object containing a representation of the C matrix.
	 */
	public String getMatrixC()
	{
		return this.matC.toString();
	}

	/** Gets string object containing the C matrix representation written	
	 * by the user.
	 *
	 * @return original string.
	 */
	public String getOriginalMatrixC()
	{
		return this.tmatC;
	}

	/** Saves the transfer function input by the user without control and
	 * validation.
	 *
	 * @param Hvar	transfer function variable.
	 * @param cst	static gain (string object).
	 * @param num	string object containing the numerator representation.
	 * @param den	string object containing the denominator representation.
	 */
	public void saveInputH(char Hvar, String cst, String num, String den)
	{
		this.H_num = num;
		this.H_den = den;
		this.H_K = cst;
		this.H_c = Hvar;
	}

	/** Saves the transfer function input by the user with control and
	 * validation (it verifies that the inputs data are correct).
	 *
	 * @param Hvar	transfer function variable.
	 * @param cst	static gain (string object).
	 * @param num	string object containing the numerator representation.
	 * @param den	string object containing the denominator representation.
	 *
	 * @throws CloudDataException if transfer fonction validation error occur.
	 */
	public void setInputH(char Hvar, String cst, String num, String den) throws CloudDataException
	{
		/* TODO :
		 *		Tester les valeurs des puissances.
		 */
		try
		{
			this.H = new TransferFunction(Hvar,cst,num,den);
		}
		catch(TransferFunctionException e)
		{
			throw new CloudDataException(e.getCode()+200, this.lang, e);
		}
		this.systemInput = CloudData.CST_I_H;

		this.H_c = Hvar;
		this.H_K = cst;
		this.H_num = num;
		this.H_den = den;

		this.ninputs = 1;
		this.noutputs = 1;
		try
		{
			if(Hvar != 'z')
				this.sysorder = this.H.getDenominator().getMax();
			else
				this.sysorder = (-this.H.getDenominator().getMin());	// TODO : à verifier pour 'z'
		}
		catch(PolynomialException e)
		{
			throw new CloudDataException(230+e.getCode(),this.lang,e);
		}
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the original transfer function variable.
	 *
	 * @return original transfer function variable.
	 */
	public char getOriginalHvariable()
	{
		return this.H_c;
	}

	/** Gets the transfer function variable.
	 *
	 * @return transfer function variable.
	 */
	public char getHvariable()
	{
		return this.H.getVar();
	}

	/** Gets the string representation of the transfer function gain (user
	 * string).
	 *
	 * @return transfer function gain (original user string).
	 */
	public String getOriginalHgain()
	{
		return this.H_K;
	}

	/** Gets the string representation of the transfer function gain.
	 *
	 * @return transfer function gain (string representation).
	 */
	public String getHgain()
	{
		return ((Double)this.H.getGain()).toString();
	}

	/** Gets the string representation of the original transfer function
	 * numerator.
	 *
	 * @return transfer function numerator (original user string).
	 */
	public String getOriginalHnumerator()
	{
		return this.H_num;
	}

	/** Gets the string representation of the transfer function numerator.
	 *
	 * @return transfer function numerator (string representation).
	 */
	public String getHnumerator()
	{
		return this.H.getStringNumerator();
	}

	/** Gets the string representation of the original transfer function
	 * denominator.
	 *
	 * @return transfer function denominator (original user string).
	 */
	public String getOriginalHdenominator()
	{
		return this.H_den;
	}

	/** Gets the string representation of the transfer function denominator.
	 *
	 * @return transfer function denominator (string representation).
	 */
	public String getHdenominator()
	{
		return this.H.getStringDenominator();
	}

	/** Saves the poles input by the user without control and verification.
	 *
	 * @param poles	system poles.
	 */
	public void saveInputPoles(String poles)
	{
		this.listpoles = poles;
	}

	/** Saves the poles input by the user with control and verification
	 * (it verifies that all the poles are correct).
	 *
	 * @param poles system poles.
	 *	
	 * @throws CloudDataException if an poles validation error occur.
	 */
	public void setInputPoles(String poles) throws CloudDataException
	{
		try
		{
			this.poles = new ListComplex(poles,',');
		}
		catch(ComplexException e)
		{
			this.poles = null;
			if(e.getCode() == 0)
			{
				// Erreur inconnu dans la liste des pôles.
				throw new CloudDataException(CloudDataException.E_POLES_UNKNOW,this.lang,e);
			}
			// Un pôle au moins est mal écrit.
			throw new CloudDataException(CloudDataException.E_POLES_PARSE,this.lang,e);
		}

		this.systemInput = CloudData.CST_I_POLE;
		this.listpoles = poles;

		this.ninputs = 1;
		this.noutputs = 1;
		this.sysorder = this.poles.size();
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets string representation of the poles list.
	 *
	 * @return string representation of the poles list.
	 */
	public String getPoles()
	{
		return this.poles.toString();
	}

	/** Gets original string representation of the poles list.
	 *
	 * @return string representation of the poles list (original user string).
	 */
	public String getOriginalPoles()
	{
		return this.listpoles;
	}

	/** Saves the alpha list input by the user without control and verification.
	 *
	 * @param alpha alpha list (coefficients of the characteristic polynomial)
	 */
	public void saveInputAlpha(String alpha)
	{
		this.listalpha = alpha;
	}

	/** Saves the alpha list input by user with control and verification
	 * (it verifies that the input data are correct).
	 *
	 * @param alpha alpha list (coefficients of the characteristic polynomial)
	 *
	 * @throws CloudDataException if an alpha validation error occur.
	 */
	public void setInputAlpha(String alpha) throws CloudDataException
	{
		try
		{
			this.alpha = new ListComplex(alpha, ',');
		}
		catch(ComplexException e)
		{
			this.alpha = null;
			if(e.getCode() == 0)
			{
				// Erreur inconnu dans la liste des alpha.
				throw new CloudDataException(CloudDataException.E_ALPHA_UNKNOW,this.lang,e);
			}
			// Un alpha au moins est mal écrit.
			throw new CloudDataException(CloudDataException.E_ALPHA_PARSE,this.lang,e);
		}
		this.systemInput = CloudData.CST_I_ALPHA;
		this.listalpha = alpha;

		this.ninputs = 1;
		this.noutputs = 1;
		this.sysorder = this.alpha.size()+1;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets a string representation of the alpha list.
	 *
	 * @return string representation of the alpha list.
	 */
	public String getAlpha()
	{
		return this.alpha.toString();
	}

	/** Gets the original string representation of the alpha list (original
	 * user string).
	 *
	 * @return string representation of the alpha list (original user string).
	 */
	public String getOriginalAlpha()
	{
		return this.listalpha;
	}

	/** Saves a linear differential equations system with his states list
	 * and inputs list without verification and control.
	 *
	 * @param states states list.
	 * @param inputs inputs list.
	 * @param equs	linear differential equations system.
	 */
	public void saveInputEqus(String states, String inputs, String equs)
	{
		this.tsys = equs;
		this.tsysstates = states;
		this.tsysinputs = inputs;
	}

	/** Saves a linear differential equations system with states list
	 * and inputs list with verification and control (it verifies that
	 * states list and inputs list are correct but also the linear differential
	 * equations system).
	 *
	 * @param states states list.
	 * @param inputs inputs list.
	 * @param equs linear differential equations system.
	 *
	 * @throws CloudDataException if an error occur in the verification of
	 * the states list, inputs list and linear differential equations system.
	 */
	public void setInputEqus(String states, String inputs, String equs) throws CloudDataException
	{
		// Validation de la liste d'états :
		try
		{		
			this.sysstates = new StateList(states,',');
		}
		catch(StateListException e)
		{
			this.sys = null;
			this.sysinputs = null;
			this.sysstates = null;

			if(e.getCode() == 0)
			{
				// Erreur inconnu sur la liste d'états.
				throw new CloudDataException(CloudDataException.E_EQL_S_UNKNOW,this.lang,e);
			}
			// Le nom d'un état ne doit pas commencé par un chiffre.
			throw new CloudDataException(CloudDataException.E_EQL_S_DIGIT,this.lang,e);
		}

		// Validation de la liste des entrées :
		try
		{
			this.sysinputs = new StateList(inputs,',');
		}
		catch(StateListException e)
		{
			this.sys = null;
			this.sysinputs = null;
			this.sysstates = null;

			if(e.getCode() == 0)
			{
				// Erreur inconnu sur la liste de sorties.
				throw new CloudDataException(CloudDataException.E_EQL_I_UNKNOW,this.lang,e);
			}
			// Le nom d'une sortie ne doit pas commencé par un chiffre.
			throw new CloudDataException(CloudDataException.E_EQL_I_DIGIT,this.lang,e);
		}

		// Validation du système :
		try
		{
			this.sys = new SysEquLin(equs,this.sysstates,this.sysinputs);
		}
		catch (SysEquLinException e)
		{
			this.sys = null;
			this.sysinputs = null;
			this.sysstates = null;

			switch(e.getCode())
			{
				case SysEquLinException.E_PARSE_D:
					// Erreur sur la lecture d'une valeur.
					throw new CloudDataException(CloudDataException.E_EQU_PARSE,this.lang,e);
				case SysEquLinException.E_PARSE_VAL:
					// Un élément est attendu avant le symbole '*'.
					throw new CloudDataException(CloudDataException.E_EQU_PARSE_V,this.lang,e);
				case SysEquLinException.E_PARSE_STT:
					// Erreur : un état est attendu après le '*'.
					throw new CloudDataException(CloudDataException.E_EQU_PARSE_S,this.lang,e);
			}
			throw new CloudDataException(CloudDataException.E_EQU_UNKNOW,this.lang,e);
		}

		// On vérifie qu'il y a autant d'équations que d'états.
		if(this.sys.getNequs() != this.sysstates.size())
		{
			this.sys = null;
			this.sysinputs = null;
			this.sysstates = null;

			// Il manque des équations.
			throw new CloudDataException(CloudDataException.E_EQU_NEQUS,this.lang);
		}

		this.systemInput = CloudData.CST_I_EQU;
				
		this.tsysstates = states;
		this.tsysinputs = inputs;
		this.tsys = equs;

		this.ninputs = this.sysinputs.size();
		this.noutputs = this.sys.getNouts();
		this.sysorder = this.sysstates.size();
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets a string containing the states list representation.
	 *
	 * @return string containing the states list representation.
	 */
	public String getStates()
	{
		return this.sysstates.toString();
	}

	/** Gets the original string containing the states list representation.
	 *
	 * @return string containing the states list representation (original
	 *		user string).
	 */
	public String getOriginalStates()
	{
		return this.tsysstates;
	}

	/** Gets a string containing the inputs list representation.
	 *
	 * @return string containing the inputs list representation.
	 */
	public String getInputs()
	{
		return this.sysinputs.toString();
	}

	/** Gets the original string containing the inputs list representation.
	 *
	 * @return string containing the inputs list representation (original
	 *		user string).
	 */
	public String getOriginalInputs()
	{
		return this.tsysinputs;
	}

	/** Gets a string containing the linear differential equations system
	 * representation.
	 *
	 * @return string containing the linear differential equations system
	 * representation.
	 */
	public String getSystem()
	{
		return this.sys.toString(this.sysstates, this.sysinputs);
	}

	/** Gets the original string containing the linear differential equations
	 * system representation.
	 *
	 * @return string containing the linear differential equations system
	 * representation (original user string).
	 */
	public String getOriginalSystem()
	{
		return this.tsys;
	}

	/** Saves the input method (how the system is input ?)
	 *
	 * @param method input method.
	 */
	public void saveInputMethod(int method)
	{
		this.systemInput = method;
	}

	/** Gets the current input method (how the system is input ?)
	 *
	 * @return input method.
	 */
	public int getInputMethod()
	{
		return this.systemInput;
	}

	/** Saves wanted poles for stabilization using poles placement
	 * without verification and control.
	 *
	 * @param poles	wanted poles for stabilization.
	 */
	public void saveStabPolesPlacement(String poles)
	{
		this.tpp_poles = poles;
	}

	/** Saves wanted poles for stabilization using poles placement
	 * with verification and control (it verifies that data input are correct).
	 *
	 * @param poles	wanted poles for stabilization.
	 *
	 * @throws CloudDataException if a pole placement error occur.
	 */
	public void setStabPolesPlacement(String poles) throws CloudDataException
	{
		try
		{
			this.pp_poles = new ListComplex(poles,',');
		}
		catch(ComplexException e)
		{
			this.poles = null;
			if(e.getCode() == 0)
			{
				// Erreur inconnu dans la liste des pôles.
				throw new CloudDataException(CloudDataException.E_S_PP_UNKNOW,this.lang,e);
			}
			// Un pôle au moins est mal écrit.
			throw new CloudDataException(CloudDataException.E_S_PP_PARSE,this.lang,e);
		}
		this.stabMethod = CloudData.CST_S_PPOLES;
		this.tpp_poles = poles;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets a string containing a poles list representation.
	 *
	 * @return string containing a poles list representation.
	 */
	public String getPolesPlacement()
	{
		return this.pp_poles.toString();
	}

	/** Gets the original string containing a poles list representation.
	 *
	 * @return string containing a poles list representation (original user string).
	 */
	public String getOriginalPolesPlacement()
	{
		return this.tpp_poles;
	}

	/** Saves wanted poles for stabilization using Ackerman
	 * without verification and control.
	 *
	 * @param poles	wanted poles for stabilization.
	 */
	public void saveStabAckerman(String poles)
	{
		this.tacker_poles = poles;
	}

	/** Saves wanted poles for stabilization using Ackerman
	 * with verification and control (it verifies that data input are correct).
	 *
	 * @param poles	wanted poles for stabilization.
	 *
	 * @throws CloudDataException if a pole placement error occur.
	 */
	public void setStabAckerman(String poles) throws CloudDataException
	{
		try
		{
			this.acker_poles = new ListComplex(poles,',');
		}
		catch(ComplexException e)
		{
			this.poles = null;
			if(e.getCode() == 0)
			{
				// Erreur inconnu dans la liste des pôles.
				throw new CloudDataException(CloudDataException.E_S_AC_UNKNOW,this.lang,e);
			}
			// Un pôle au moins est mal écrit.
			throw new CloudDataException(CloudDataException.E_S_AC_PARSE,this.lang,e);
		}
		this.stabMethod = CloudData.CST_S_ACKER;
		this.tacker_poles = poles;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets a string containing a poles list representation.
	 *
	 * @return string containing a poles list representation.
	 */
	public String getAckerman()
	{
		return this.acker_poles.toString();
	}

	/** Gets the original string containing a poles list representation.
	 *
	 * @return string containing a poles list representation (original user string).
	 */
	public String getOriginalAckerman()
	{
		return this.tacker_poles;
	}

	/** Saves wanted poles for stabilization using Brunovsky
	 * without verification and control.
	 *
	 * @param poles	wanted poles for stabilization.
	 */
	public void saveStabBrunovsky(String poles)
	{
		this.tbru_poles = poles;
	}

	/** Saves wanted poles for stabilization using Brunovsky
	 * with verification and control (it verifies that data input are correct).
	 *
	 * @param poles	wanted poles for stabilization.
	 *
	 * @throws CloudDataException if a pole placement error occur.
	 */
	public void setStabBrunovsky(String poles) throws CloudDataException
	{
		try
		{
			this.bru_poles = new ListComplex(poles,',');
		}
		catch(ComplexException e)
		{
			this.poles = null;
			if(e.getCode() == 0)
			{
				// Erreur inconnu dans la liste des pôles.
				throw new CloudDataException(CloudDataException.E_S_BR_UNKNOW,this.lang,e);
			}
			// Un pôle au moins est mal écrit.
			throw new CloudDataException(CloudDataException.E_S_BR_PARSE,this.lang,e);
		}
		this.stabMethod = CloudData.CST_S_BRUNOV;
		this.tbru_poles = poles;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets a string containing a poles list representation.
	 *
	 * @return string containing a poles list representation.
	 */
	public String getBrunovsky()
	{
		return this.bru_poles.toString();
	}

	/** Gets the original string containing a poles list representation.
	 *
	 * @return string containing a poles list representation (original user string).
	 */
	public String getOriginalBrunovsky()
	{
		return this.tbru_poles;
	}

	/** Saves the stabilization method (how the system is stabilized ?)
	 *
	 * @param method stabilization method.
	 */
	public void saveStabMethod(int method)
	{
		this.stabMethod = method;
	}

	/** Gets the stabilization method (how the system is stabilized ?)
	 *
	 * @return stabilization method.
	 */
	public int getStabMethod()
	{
		return this.stabMethod;
	}

	/** Saves wanted output equations for tracking (pursuit) without
	 * verification and control.
	 *
	 * @param outputequs wanted output equations.
	 * @param lambdalist lambda lists (convergence coefficient).
	 * @param useStab define if it uses the stabilized system (true) or not (false).
	 */
	public void savePursuitStd(String outputequs, String lambdalist, boolean useStab)
	{
		this.toequs = outputequs;
		this.useStab = useStab;
		this.tolambda = lambdalist;
	}

	/** Saves wanted output equations for tracking (pursuit) with
	 * verification and control (it verifies that data input are correct).
	 *
	 * @param outputequs wanted output equations.
	 * @param lambdalist lambda lists (convergence coefficient).
	 * @param useStab define if it uses the stabilized system (true) or not (false).
	 *
	 * @throws CloudDataException if an error occur in the tracking definition.
	 */
	public void setPursuitStd(String outputequs, String lambdalist, boolean useStab) throws CloudDataException
	{
		// TODO Test
		try
		{
			this.olambda = new ListLambda(lambdalist);
		}
		catch(ComplexException e)
		{
			this.olambda = null;
			this.oequs = null;
			throw new CloudDataException(2100+e.getCode(), this.lang, e);
		}
		
		try
		{
			this.oequs = new PursuitEqus(outputequs);
		}
		catch(PursuitEqusException e)
		{
			this.olambda = null;
			this.oequs = null;
			throw new CloudDataException(2110+e.getCode(),this.lang,e);
		}

		this.pursuitMethod = CloudData.CST_P_DEFAULT;
		this.useStab = useStab;

		this.toequs = outputequs;
		this.tolambda = lambdalist;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the original string containing wanted output equations.
	 *
	 * @return original string containing wanted output equations (original user string).
	 */
	public String getOriginalStd()
	{
		return this.toequs;
	}

	/** Gets a string containing wanted output equations.
	 *
	 * @return original string containing wanted output equations.
	 */
	public String getStd()
	{
		return this.oequs.toString();
	}

	/** Sets the useStab variable.
	 *
	 * @param useStab true if it use stabilized system and else false.
	 */
	public void setUseStab(boolean useStab)
	{
		this.useStab = useStab;
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the useStab variable.
	 *
	 * @return return true if it use stabilized system and else false.
	 */
	public boolean getUseStab()
	{
		return this.useStab;
	}

	/** Saves pursuit (tracking) method.
	 *
	 * @param method tracking method.
	 */
	public void savePursuitMethod(int method)
	{
		this.pursuitMethod = method;
	}

	/** Gets pursuit (tracking) method.
	 *
	 * @return tracking method.
	 */
	public int getPursuitMethod()
	{
		return this.pursuitMethod;
	}

	/** Computes the stabilization.
	 *
	 *  @throws CloudDataException if a write error occurs
	 */
	public void setStab() throws CloudDataException
	{
		this.doStab();
	}

	/** Computes the stabilization.
	 *
	 *  @throws CloudDataException if a write error occurs
	 */
	public void doStab() throws CloudDataException
	{
		int npoles = -1;
		// On créer un PropertyFile pour y mettre les données
		PropertyFile p = new PropertyFile();

		p.add_nt("SystemInput", ((Integer)this.systemInput).toString() + ";");
		p.add_nt("StabMethod", ((Integer)this.stabMethod).toString() + ";");

		switch(this.systemInput)
		{
			/** Matrices (A, B and C). */
			case CloudData.CST_I_MAT:
				p.add_nt("matA", "[" + this.matA.toString() + "];");
				p.add_nt("matB", "[" + this.matB.toString() + "];");
				p.add_nt("matC", "[" + this.matC.toString() + "];");
				p.add_nt("matD", "[0.0]");
				break;

			/** System poles list.  */
			case CloudData.CST_I_POLE:
				p.add_nt("poles", "[" + this.poles.toScilab() + "];");
				break;

			/** Coefficients list of the characteristic polynomial */
			case CloudData.CST_I_ALPHA:
				p.add_nt("alpha", "[" + this.alpha.toScilab() + "];");
				break;

			/** Linear differential equations system */
			case CloudData.CST_I_EQU:
				p.add_nt("matA", "[" + this.sys.toStringMatrixFormA(this.sysstates) + "];");
				p.add_nt("matB", "[" + this.sys.toStringMatrixFormB(this.sysinputs) + "];");
				p.add_nt("matC", "[" + this.sys.toStringMatrixFormC(this.sysstates) + "];");
				p.add_nt("matD", "[" + this.sys.toStringMatrixFormD(this.sysinputs) + "];");
			break;

			/** Transfer function */
			case CloudData.CST_I_H:
				p.add_nt("H_K", ((Double)this.H.getGain()).toString() + ";");
				p.add_nt("H_c", "\"" + ((Character)this.H.getVar()).toString() + "\";");
				try
				{
					p.add_nt("H_num", "[" + this.H.getCoefNumerator() + "];");
					p.add_nt("H_den", "[" + this.H.getCoefDenominator() + "];");
				}
				catch(TransferFunctionException e)
				{
					throw new CloudDataException(200+e.getCode(), this.lang,e);
				}
			break;		
		}

		switch(this.stabMethod)
		{
			/** Poles placement */
			case CloudData.CST_S_PPOLES:
				p.add_nt("stabpole", "[" + this.pp_poles.toScilab() + "];");
				npoles = pp_poles.size();
				break;

			/** Ackerman method */
			case CloudData.CST_S_ACKER:
				p.add_nt("stabpole", "[" + this.acker_poles.toScilab() + "];");
				npoles = acker_poles.size();
				break;

			/** Brunovsky method */
			case CloudData.CST_S_BRUNOV:
				p.add_nt("stabpole", "[" + this.bru_poles.toScilab() + "];");
				npoles = bru_poles.size();
				break;
		}

		if (this.sysorder !=  npoles)
		{
			throw new CloudDataException(CloudDataException.E_S_ORDER, this.lang);
		}

		// On essaye d'enregistrer le PropertyFile crée.
		try
		{
			p.save("Scilab/data_stab.sce");
		}
		catch(PropertyFileException e)
		{
			throw new CloudDataException(10+e.getCode(),this.lang,e);
		}

		// Supprime le fichier de sortie si il existe déjà.


		java.io.File f = new java.io.File("Scilab/dataSout.sce");
		if (f.exists())
		{
			f.delete();
		}
		f = new java.io.File("Scilab/dataSout.end");
		if (f.exists())
		{
			f.delete();
		}

		try
		{
			Runtime.getRuntime().exec(this.pathScilab + " -f Scilab/stab.sce -nw");
		}
		catch(Exception e)
		{
			// TODO Erreur exec.
			e.printStackTrace();
		}
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Sees if the output file (for stabilization) exists
	 *
	 * @return false if the file does not exist and else true.
	 */
	public boolean isStabFinished()
	{
		java.io.File pfile = new java.io.File("Scilab/dataSout.end");
		return pfile.exists();
	}

	/** Gets the values returned by Scilab in the output file.
	 *
	 */
	public void getStabOutput() throws CloudDataException
	{
		// TODO Gestion erreur Scilab
		int error = -1;
		PropertyFile p;
		java.io.File pfile = new java.io.File("Scilab/dataSout.sce");
		if (!pfile.exists())
		{
			throw new CloudDataException(CloudDataException.E_S_SCILAB,this.lang);
		}

		// On lit le fichier :
		try
		{
			p = new PropertyFile("Scilab/dataSout.sce");
		}
		catch(PropertyFileException e)
		{
			throw new CloudDataException(10+e.getCode(),this.lang,e);
		}
		try
		{
			error = Integer.parseInt(p.getString("error"));
		}
		catch(NumberFormatException e)
		{
			throw new CloudDataException(22,this.lang,e);
			// TODO : Créer une nouvelle erreur spécifique !
		}

		if (error != 0)
		{
			throw new CloudDataException(CloudDataException.E_UNKNOW, "Erreur dans le traitement Scilab !", this.lang);
		}
		
		if(p.exist("As") && p.exist("Bs") && p.exist("Cs"))
		{
			String A = p.getString("As");
			if (A.charAt(0) == '[')
			{
				A = A.substring(1,A.length()-1);
			}
			String B = p.getString("Bs");
			if (B.charAt(0) == '[')
			{
				B = B.substring(1,B.length()-1);
			}
			String C = p.getString("Cs");
			if (C.charAt(0) == '[')
			{
				C = C.substring(1,C.length()-1);
			}
			String K = p.getString("K");
			if (K.charAt(0) == '[')
			{
				K = K.substring(1,K.length()-1);
			}
			this.setStabMatrices(A,B,C,K);
		}
		else
		{
			System.out.println("Erreur : matrice pas toute définit");
			// TODO Erreur : les matrices ne sont pas toutes définis
		}

		java.io.File f = new java.io.File("Scilab/dataSout.end");
		if (f.exists())
		{
			f.delete();
		}
	}

	/** Sets the stabilized matrices
	 *
	 * @param stabA stabilized A matrix
	 * @param stabB stabilized B matrix
	 * @param stabC stabilized C matrix
	 *
	 * @throws CloudDataException if a matrix validation error occur.
	 */
	// TODO change error messages.
	public void setStabMatrices(String stabA, String stabB, String stabC, String stabK) throws CloudDataException
	{
		int n;	// Ordre du système.

		// On traite la matrice A :
		try
		{
			stabA = stabA.replaceAll("D","e");
			this.stabA = new SciMatrix(stabA);
		}
		catch(SciMatrixException e)
		{
			this.stabA = null;
			this.stabB = null;
			this.stabC = null;
			this.stabK = null;
			switch(e.getCode())
			{
				case SciMatrixException.E_ILL_ROWS:
					// Dimension de A incohérente.
					throw new CloudDataException(CloudDataException.E_MAT_A_DIMERR, this.lang,e);
				case SciMatrixException.E_NOTXT:
					// A est vide.
					throw new CloudDataException(CloudDataException.E_MAT_A_EMPTY, this.lang,e);
				case SciMatrixException.E_PARSE_VAL:
					// Une des valeurs de A est mal écrite.
					throw new CloudDataException(CloudDataException.E_MAT_A_PARSE, this.lang,e);
			}
			// La matrice A n'est pas correct.
			throw new CloudDataException(CloudDataException.E_MAT_A_UNKNOW,this.lang,e);
		}

		// On traite la matrice B
		try
		{
			stabB = stabB.replaceAll("D","e");
			this.stabB = new SciMatrix(stabB);
		}
		catch(SciMatrixException e)
		{
			this.stabA = null;
			this.stabB = null;
			this.stabC = null;
			this.stabK = null;
			switch(e.getCode())
			{
				case SciMatrixException.E_ILL_ROWS:
					// Dimension de B incohérente.
					throw new CloudDataException(CloudDataException.E_MAT_B_DIMERR, this.lang,e);
				case SciMatrixException.E_NOTXT:
					// B est vide.
					throw new CloudDataException(CloudDataException.E_MAT_B_EMPTY, this.lang,e);
				case SciMatrixException.E_PARSE_VAL:
					// Une des valeurs de B est mal écrite.
					throw new CloudDataException(CloudDataException.E_MAT_B_PARSE, this.lang,e);
			}
			// La matrice B n'est pas correct.
			throw new CloudDataException(CloudDataException.E_MAT_B_UNKNOW,this.lang,e);
		}

		// On traite la matrice C
		try
		{
			stabC = stabC.replaceAll("D","e");
			this.stabC = new SciMatrix(stabC);
		}
		catch(SciMatrixException e)
		{
			this.stabA = null;
			this.stabB = null;
			this.stabC = null;
			this.stabK = null;
			switch(e.getCode())
			{
				case SciMatrixException.E_ILL_ROWS:
					// Dimension de C incohérente.
					throw new CloudDataException(CloudDataException.E_MAT_C_DIMERR, this.lang,e);
				case SciMatrixException.E_NOTXT:
					// C est vide.
					throw new CloudDataException(CloudDataException.E_MAT_C_EMPTY, this.lang,e);
				case SciMatrixException.E_PARSE_VAL:
					// Une des valeurs de C est mal écrite.
					throw new CloudDataException(CloudDataException.E_MAT_C_PARSE, this.lang,e);
			}
			// La matrice C n'est pas correct.
			throw new CloudDataException(CloudDataException.E_MAT_C_UNKNOW,this.lang,e);
		}

		// On traite la matrice K
		try
		{
			stabK = stabK.replaceAll("D","e");
			this.stabK = new SciMatrix(stabK);
		}
		catch(SciMatrixException e)
		{
			this.stabA = null;
			this.stabB = null;
			this.stabC = null;
			this.stabK = null;
			switch(e.getCode())
			{
				case SciMatrixException.E_ILL_ROWS:
					// Dimension de K incohérente.
					throw new CloudDataException(CloudDataException.E_MAT_K_DIMERR, this.lang,e);
				case SciMatrixException.E_NOTXT:
					// K est vide.
					throw new CloudDataException(CloudDataException.E_MAT_K_EMPTY, this.lang,e);
				case SciMatrixException.E_PARSE_VAL:
					// Une des valeurs de K est mal écrite.
					throw new CloudDataException(CloudDataException.E_MAT_K_PARSE, this.lang,e);
			}
			// La matrice K n'est pas correct.
			throw new CloudDataException(CloudDataException.E_MAT_K_UNKNOW,this.lang,e);
		}
		
		// On vérifie que la matrice A est bien carré (n*n).
		if ( (n = this.matA.getRows()) != this.matA.getColumns())
		{
			this.stabA = null;
			this.stabB = null;
			this.stabC = null;
			// La matrice A n'est pas carré.
			throw new CloudDataException(CloudDataException.E_MAT_A_NOSQRT,this.lang);
		}

		// On vérifie que B a bien n lignes.
		if(n != this.matB.getRows())
		{
			this.stabA = null;
			this.stabB = null;
			this.stabC = null;
			// Le nombre de lignes de B est différent de l'ordre du système.
			throw new CloudDataException(CloudDataException.E_MAT_B_DIFORD,this.lang);
		}

		// On vérifie que C a bien n colonnes.
		if(n != this.matC.getColumns())
		{
			this.stabA = null;
			this.stabB = null;
			this.stabC = null;
			// Le nombre de colonnes de C est différent de l'ordre du système.
			throw new CloudDataException(CloudDataException.E_MAT_C_DIFORD,this.lang);
		}
		this.stabilized = true;
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Gets the string representation of the stabilized A matrix.
	 *
	 *	@return string representation of As.
	 */
	public String getStabA()
	{
		return this.stabA.toString();
	}

	/** Gets the string representation of the stabilized B matrix.
	 *
	 *	@return string representation of Bs.
	 */
	public String getStabB()
	{
		return this.stabB.toString();
	}

	/** Gets the string representation of the stabilized C matrix.
	 *
	 *	@return string representation of Cs.
	 */
	public String getStabC()
	{
		return this.stabC.toString();
	}

	/** Gets the string representation of K matrix (feedback matrix).
	 *
	 *	@return string representation of K.
	 */
	public String getStabK()
	{
		return this.stabK.toString();
	}

	/** Compute the tracking.
	 *
	 */
	public void setPursuit()
	{
		this.doPursuit();
	}

	/** Compute the tracking.
	 *
	 */
	public void doPursuit()
	{
		// TODO setPursuit
		
		//Il y a eu un changement dans CloudData donc
		this.isSaved = 1;
	}

	/** Sees if the output file (for Pursuit/Tracking) exists
	 *
	 * @return false if the file does not exist and else true.
	 */
	public boolean isPursuitFinished()
	{
		java.io.File pfile = new java.io.File("dataPout.sce");
		return pfile.exists();
	}

	/** Gets the values returned by Scilab in the output file.
	 *
	 */
	public void getPursuitOutput()
	{
		// TODO
	} 

	/** Does the stabilization simulation.
	 *
	 *	@param ti initial time.
	 *	@param tf final time.
	 *	@param step simulation step.
	 *	@param val specific value.
	 *	@param state to plot.
	 *	@param method input method
	 */
	public void doSimStab(String ti, String tf, String step, String val, String state, int method) throws CloudDataException
	{
		// TODO À tester
		if (this.stabilized)
		{
			// On créer un PropertyFile pour y mettre les données
			PropertyFile p = new PropertyFile();

			int typeout = 0;
			int nout = 0;

			try
			{
				int iti = Integer.parseInt(ti);
			}
			catch(NumberFormatException e)
			{
				// Erreur sur ti.
				throw new CloudDataException(CloudDataException.E_S_SIM_TI, this.lang, e);
			}

			try
			{
				int itf = Integer.parseInt(tf);
			}
			catch(NumberFormatException e)
			{
				// Erreur sur tf.
				throw new CloudDataException(CloudDataException.E_S_SIM_TF, this.lang, e);
			}

			try
			{
				int ival = Integer.parseInt(val);
			}
			catch(NumberFormatException e)
			{
				// Erreur sur val.
				throw new CloudDataException(CloudDataException.E_S_SIM_PARAM, this.lang, e);
			}

			if ((method <= 0) || (method >= 4))
			{
				// Erreur : methode incorrecte.
				throw new CloudDataException(CloudDataException.E_S_SIM_METHOD, this.lang);
			}

			state = state.trim();
			if (state.charAt(0) == 'y')
			{
				typeout = 1;
			}
			else if (state.charAt(0) == 'x')
			{
				typeout = 0;
			}
			else
			{
				// Erreur : type de sortie incorrecte.
				throw new CloudDataException(CloudDataException.E_S_SIM_TYPEO, this.lang);
			}

			state = state.substring(1,state.length());
			try
			{
				nout = Integer.parseInt(state);
			}
			catch(NumberFormatException e)
			{
				// Erreur sur le numéro de la sortie.
				throw new CloudDataException(CloudDataException.E_S_SIM_NOUT, this.lang, e);
			}

			p.add_nt("SystemInput","1;");
			p.add_nt("matA", "["+this.stabA.toString()+"];");
			p.add_nt("matB", "["+this.stabB.toString()+"];");
			p.add_nt("matC", "["+this.stabC.toString()+"];");
			p.add_nt("matD", "[0.0];");
			p.add_nt("SimInput", ((Integer)method).toString() + ";");
			p.add_nt("amp", val + ";");
			p.add_nt("ti", ti + ";");
			p.add_nt("tf", tf + ";");
			p.add_nt("step", step + ";");
			p.add_nt("typeout", ((Integer)typeout).toString() + ";");
			p.add_nt("nout", ((Integer)nout).toString() + ";");

			// On essaye d'enregistrer le PropertyFile crée.
			try
			{
				p.save("Scilab/data_simstab.sce");
			}
			catch(PropertyFileException e)
			{
				throw new CloudDataException(10+e.getCode(),this.lang,e);
			}

			try
			{
				Runtime.getRuntime().exec(this.pathScilab + " -f Scilab/simstab.sce -nw");
			}
			catch(Exception e)
			{
				// TODO Erreur exec.
				e.printStackTrace();
			}
		}
		else
		{
			// Erreur : Le système n'est pas stabilisé.
			throw new CloudDataException(CloudDataException.E_S_SIM_NSTAB, this.lang);
		}
	}

	/** Does the tracking (pursuit) simulation.
	 *
	 *	@param ti initial time.
	 *	@param tf final time.
	 *	@param step simulation step.
	 */
	public void doSimPursuit(String ti, String tf, String step, String val)// throws CloudDataException 
	{	
		// TODO
	}

	/** Saves data contained in the CloudData object.
	 *
	 * @param pfile	String containing the access directory to the file 
	 *		in which the data will be saved.
	 *
	 * @throws CloudDataException if a write error occurs.
	 */
	public void save(String pfile) throws CloudDataException
	{
		try
		{
			this.save(new java.io.File(pfile));
		}
		catch(NullPointerException e)
		{
			throw new CloudDataException(21,this.lang,e);
		}
	}

	/** Saves data contained in the CloudData object.
	 *
	 * @param pfile Reference to the file in which the data will be saved.
	 *
	 * @throws CloudDataException if a write error occurs.
	 */
	public void save(java.io.File pfile) throws CloudDataException
	{
		// On spécifie que le projet a bien été enregistré
		this.isSaved = 0;

		// On créer un PropertyFile pour y mettre les données
		PropertyFile p = new PropertyFile();

		// Methods
		p.add_nt("systemInput", ((Integer)this.systemInput).toString());
		p.add_nt("stabMethod", ((Integer)this.stabMethod).toString());
		p.add_nt("pursuitMethod", ((Integer)this.pursuitMethod).toString());
		
		// Transfer function data
		p.add_nt("tH_K", this.H_K);
		p.add_nt("tH_c", ((Character)this.H_c).toString());
		p.add_nt("tH_num", this.H_num);
		p.add_nt("tH_den", this.H_den);
		if(this.H != null)
		{
			p.add_nt("H_K", ((Double)this.H.getGain()).toString());
			p.add_nt("H_c", ((Character)this.H.getVar()).toString());
			p.add_nt("H_num", this.H.getStringNumerator());
			p.add_nt("H_den", this.H.getStringDenominator());
		}

		// Poles data
		p.add_nt("listpoles", this.listpoles);
		if(this.poles != null)
			p.add_nt("poles", this.poles.toString());

		// Alpha data
		p.add_nt("listalpha", this.listalpha);
		if(this.alpha != null)
			p.add_nt("alpha", this.alpha.toString());

		// Matrices data
		p.add_nt("tmatA", this.tmatA);
		if(this.matA != null)
			p.add_nt("matA", this.matA.toString());
		p.add_nt("tmatB", this.tmatB);
		if(this.matB != null)
			p.add_nt("matB", this.matB.toString());
		p.add_nt("tmatC", this.tmatC);
		if(this.matC != null)
			p.add_nt("matC", this.matC.toString());

		// Linear differential equations system data
		p.add_nt("tsysstates", this.tsysstates);
		if(this.sysstates != null)
			p.add_nt("sysstates", this.sysstates.toString());
		p.add_nt("tsysinputs", this.tsysinputs);
		if(this.sysinputs != null)
			p.add_nt("sysinputs", this.sysinputs.toString());
		// replaceAll : vérifier que le "\n" fonctionne bien sur tout les OS
		//	quelque soit la manière dont est encodé le saut de ligne.
		p.add_nt("tsys", this.tsys.replaceAll("\n", "#1"));
		if(this.sys != null)
			p.add_nt("sys", this.sys.toString(this.sysstates, this.sysinputs, "#1"));

		// Poles placement data
		p.add_nt("tpp_poles", this.tpp_poles);
		if(this.pp_poles != null)
			p.add_nt("pp_poles", this.pp_poles.toString());

		// Ackerman data
		p.add_nt("tacker_poles", this.tacker_poles);
		if(this.acker_poles != null)
			p.add_nt("acker_poles", this.acker_poles.toString());

		// Brunovsky data
		p.add_nt("tbru_poles", this.tbru_poles);
		if(this.bru_poles != null)
			p.add_nt("bru_poles", this.bru_poles.toString());

		// Std data (Tracking/Pursuit)
		p.add_nt("toequs",this.toequs);
			// TODO add object

		// stabilized ?
		if(this.stabilized)
			p.add_nt("stabilized","true");
		else
			p.add_nt("stabilized","false");

		// tracked ?
		if(this.tracked)
			p.add_nt("tracked","true");
		else
			p.add_nt("tracked","false");

		// useStab ? (Use stabilization ?)
		if(this.useStab)
			p.add_nt("useStab","true");
		else
			p.add_nt("useStab","false");

		// Stabilized matrices
		if( (this.stabA != null) && (this.stabB != null) && (this.stabC != null) && (this.stabK != null))
		{
			p.add_nt("stabA", this.stabA.toString());
			p.add_nt("stabB", this.stabB.toString());
			p.add_nt("stabC", this.stabC.toString());
			p.add_nt("stabK", this.stabK.toString());
		}
			

		// On essaye d'enregistrer le PropertyFile crée.
		try
		{
			p.save(pfile);
		}
		catch(PropertyFileException e)
		{
			throw new CloudDataException(10+e.getCode(),this.lang,e);
		}	
	}

	/** Initializes the CloudData with data contained in a file.
	 *
	 * @param pfile string containing the access directory to the file to load.
	 *
	 * throws CloudDataException if a read error occurs.
	 */
	public void load(String pfile) throws CloudDataException
	{
		try
		{
			this.load(new java.io.File(pfile));
		}
		catch(NullPointerException e)
		{
			throw new CloudDataException(21,this.lang,e);
		}
	}

	/** Initializes the CloudData with data contained in a file.
	 *
	 * @param pfile reference to the file to load.
	 *
	 * @throws CloudDataException if a read error occurs.
	 */
	public void load(java.io.File pfile) throws CloudDataException
	{
		PropertyFile p = null;
		String tmp = "";
		// On lit le fichier :
		try
		{
			p = new PropertyFile(pfile);
		}
		catch(PropertyFileException e)
		{
			throw new CloudDataException(10+e.getCode(),this.lang,e);
		}

		// Methods : systemInput
		try
		{
			this.systemInput = Integer.parseInt(p.getString("systemInput"));
		}
		catch(NumberFormatException e)
		{
			throw new CloudDataException(22,this.lang,e);
		}

		// Methods : stabMethod
		try
		{
			this.systemInput = Integer.parseInt(p.getString("stabMethod"));
		}
		catch(NumberFormatException e)
		{
			throw new CloudDataException(22,this.lang,e);
		}

		// Methods : pursuitMethod
		try
		{
			this.systemInput = Integer.parseInt(p.getString("pursuitMethod"));
		}
		catch(NumberFormatException e)
		{
			throw new CloudDataException(22,this.lang,e);
		}

		// Gets transfer function
		if(p.exist("H_K") && p.exist("H_c") && p.exist("H_num") && p.exist("H_den"))
		{
			this.setInputH(p.getString("H_c").charAt(0), p.getString("H_K"), p.getString("H_num"), p.getString("H_den"));
		}
		this.H_K = p.getString("tH_K");
		this.H_c = p.getString("tH_c").charAt(0);
		this.H_num = p.getString("tH_num");
		this.H_den = p.getString("tH_den");

		// Gets poles
		if(p.exist("poles"))
		{
			this.setInputPoles(p.getString("poles"));
		}
		else
			this.poles = null;
		this.listpoles = p.getString("listpoles");

		// Gets alpha
		if(p.exist("alpha"))
		{
			this.setInputAlpha(p.getString("alpha"));
		}
		else
			this.alpha = null;
		this.listalpha = p.getString("listalpha");

		// Gets matrices
		if(p.exist("matA") && p.exist("matB") && p.exist("matC"))
		{
			this.setInputMatrix(p.getString("matA"),p.getString("matB"),p.getString("matC"));
		}
		else
		{
			this.matA = null;
			this.matB = null;
			this.matC = null;
		}
		this.tmatA = p.getString("tmatA");
		this.tmatB = p.getString("tmatB");
		this.tmatC = p.getString("tmatC");

		// Get linear differential equations system
		if(p.exist("sysstates") && p.exist("sysinputs") && p.exist("sys"))
		{
			this.setInputEqus(p.getString("sysstates"),p.getString("sysinputs"),p.getString("sys","\n"));
		}
		else
		{
			this.sysstates = null;
			this.sysinputs = null;
			this.sys = null;
		}
		this.tsysstates = p.getString("tsysstates");
		this.tsysinputs = p.getString("tsysinputs");
		this.tsys = p.getString("tsys", "\n");
		
		// Gets poles placement
		if(p.exist("pp_poles"))
		{
			this.setStabPolesPlacement(p.getString("pp_poles"));
		}
		else
		{
			this.pp_poles = null;
		}
		this.tpp_poles = p.getString("tpp_poles");

		// Gets Ackerman
		if(p.exist("acker_poles"))
		{
			this.setStabAckerman(p.getString("acker_poles"));
		}
		else
		{
			this.acker_poles = null;
		}
		this.tpp_poles = p.getString("tacker_poles");

		// Gets Brunovsky
		if(p.exist("bru_poles"))
		{
			this.setStabBrunovsky(p.getString("bru_poles"));
		}
		else
		{
			this.bru_poles = null;
		}
		this.tpp_poles = p.getString("tbru_poles");

		// Get Std (tracking/pursuit)
			// TODO get object
		this.toequs = p.getString("toequs");
			

		// stabilized ?
		tmp = p.getString("stabilized");
		if(tmp.charAt(0) == 't')
			this.stabilized = true;
		else
			this.stabilized = false;

		// tracked
		tmp = p.getString("tracked");
		if(tmp.charAt(0) == 't')
			this.tracked = true;
		else
			this.tracked = false;

		// useStab ? (Use stabilization ?)
		tmp = p.getString("useStab");
		if(tmp.charAt(0) == 't')
			this.useStab = true;
		else
			this.useStab = false;

		// Stabilized matrices
		if(p.exist("stabA") && p.exist("stabB") && p.exist("stabC") && p.exist("stabK"))
		{
			this.setStabMatrices(p.getString("stabA"),p.getString("stabB"),p.getString("stabC"),p.getString("stabK"));
		}
		
	}
	
}

