/*	PursuitEqus.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Handles a pursuit/tracking equation.
 */

package PursuitEqus;

/** Handles a pursuit/tracking equation.
 */
public class PEBase
{
	/** Output id of the equation. */
	private int id = 0;
	/** Expression of the equation. */
	private String equ = "";

	/** Initializes a newly created PEBase with an id and an equation.
	 *
	 * @param id equation id.
	 * @param equ equation expression.
	 */
	public PEBase(int id, String equ)
	{
		this.id = id;
		this.equ = equ;
	}

	/** Initializes a newly created PEBase with a string containing the
	 * equation representation.
	 *
	 * @param tequ equation representation.
	 *
	 * @throws PursuitEqusException if a parse error occur.
	 *
	 * @see PursuitEqus#fromString(String) fromString(String tequ)
	 */
	public PEBase(String tequ) throws PursuitEqusException
	{
		this.fromString(tequ);
	}

	/** Sets id and equation with a string containing the equation
	 * representation.
	 *
	 * @param tequ equation representation.
	 *
	 * @throws PursuitEqusException if a parse error occur.
	 */
	public void fromString(String tequ) throws PursuitEqusException
	{
		String tmp[] = tequ.split(":=");
		String var = tmp[0].trim();
		this.equ = tmp[1].trim();

		if(var.charAt(0) != 'y')
		{
			// Erreur : Pas la bonne variable ! (nécessite y)
			throw new PursuitEqusException(PursuitEqusException.E_OUTVAR, var);
		}
		else
		{
			var = var.substring(1);
			try
			{
				this.id = Integer.parseInt(var);
			}
			catch(NumberFormatException e)
			{
				throw new PursuitEqusException(PursuitEqusException.E_IDPARSE, var,e);
			}
		}
	}

	/** Gets the equation id.
	 *
	 * @return equation id.
	 *
	 */
	public int getId()
	{
		return this.id;
	}

	/** Sets the equation id.
	 *
	 * @param id equation id.
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/** Gets the equation expression.
	 *
	 * @return equation expression.
	 */
	public String getEqu()
	{
		return this.equ;
	}

	/** Sets the equation expression.
	 *
	 * @param equ equation expression.
	 */
	public void setEqu(String equ)
	{
		this.equ = equ;
	}

	/** Returns a string object representation containing the equation.
	 *
	 * @return string representation of the equation.
	 */
	public String toString()
	{
		return "y" + this.id + " := " + this.equ;
	}
}






















