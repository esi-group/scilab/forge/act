/*	ListComplex.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Allows to handle a complex number list and parse a string containing
 * a representation of a complex number list.
 */

package Complex;

/** Allows to handle a complex number list and parse a string containing
 * a representation of a complex number list.
 */
public class ListComplex
{

	/** Complex number list */
	java.util.ArrayList<Complex> list;

	/** Initializes a newly created complex number list with an empty list.
	 */
	public ListComplex()
	{
		this.list = new java.util.ArrayList<Complex>();
	}

	/** Initializes a newly created complex number list with a string 
	 * representation of a complex number list.
	 *
	 * @param slist string representation of the complex number list.
	 * @param sep separator character used to separate the complex numbers.
	 *
	 * @throws ComplexException if a parse error occur.
	 */
	public ListComplex(String slist, char sep) throws ComplexException
	{
		this();
		this.parseList(slist,sep);
	}

	/** Initializes a newly created complex number list with a string
	 * representation of a complex number list. The separator character
	 * must be ','.
	 *
	 * @param slist string representation of the complex number list.
	 *
	 * @throws ComplexException if a parse error occur.
	 */
	public ListComplex(String slist) throws ComplexException
	{
		this(slist, ',');
	}

	/** Adds a complex number in the list.
	 *
	 * @param elem element added in the list.
	 */
	public void add(Complex elem)
	{
		this.list.add(elem);
	}

	/** Gets the list size.
	 *
	 * @return return the number of element in the list.
	 */
	public int size()
	{
		return this.list.size();
	}

	/** Sets an element at the given position.
	 *
	 * @param i position of the modified element.
	 * @param elem element set at position i.
	 */
	public void set(int i, Complex elem)
	{
		this.list.set(i, elem);
	}

	/** Gets an element at the given position
	 *
	 * @param i position of the element.
	 *
	 * @return complex number.
	 */
	public Complex get(int i)
	{
		return this.list.get(i);
	}

	/** Parses the string containing a complex number list representation 
	 * with ',' as separator character.
	 *
	 * @param slist : string containing a complex number list representation.
	 *
	 * @throws ComplexException if a parse error occur.
	 */
	public void parseList(String slist) throws ComplexException
	{
		this.parseList(slist, ',');
	}

	/** Parses the string containing a complex number list representation 
	 * with an user defined separator character.
	 *
	 * @param slist string containing a complex number list representation.
	 * @param sep separator character. For example : ',' ';' ';' (no special character !).
	 *
	 * @throws ComplexException if a parse error occur.
	 */
	public void parseList(String slist, char sep) throws ComplexException
	{
		String tmp = "";
		int l = slist.length();
		for (int i = 0; i < l ; i++)
		{
			if (slist.charAt(i) > 32)
			{
				if(slist.charAt(i) != sep)
					tmp += slist.charAt(i);
				else
				{
					this.list.add(new Complex(tmp));
					tmp = "";
				}
			}
		}
		if(tmp.length() > 0)
			this.list.add(new Complex(tmp));
	}

	/** Returns a string object representing this complex number list.
	 *
	 * @param cplxvar complex variable.
	 *
	 * @return a string representation of the complex number list.
	 */
	public String toString(String cplxvar)
	{
		String ret = "";
		int l = this.list.size();
		for(int i = 0; i < l ; i++)
		{
			if(i > 0)
				ret += ", ";
			ret += this.list.get(i).toString(cplxvar);		
		}
		return ret;
	}

	/** Returns a string object representing this complex number list.
	 *
	 * @return a string representation of the complex number list.
	 */
	public String toString()
	{
		return this.toString("i");
	}

	/** Returns a string object representing this complex number list
	 * (compatible with Scilab).
	 *
	 * @return a string representation of the complex number list.
	 */
	public String toScilab()
	{
		return this.toString("*%i");
	}

}











