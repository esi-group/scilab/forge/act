/*	PolynomialException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Thrown to indicate a error when using a Polynomial object or a PNome object.
 */

package Polynomial;

/** Thrown to indicate a error when using a Polynomial object or a PNome object.
 *
 */
public class PolynomialException extends Exception
{
	/** [Polynomial] Unknow error. */
	public final static int E_UNKNOW = 0;
	/** [PNome] number parse error. */		
	public final static int E_M_PARSE_N = 1;
	/** [PNome] '^' is expected after the variable. */
	public final static int E_M_POWNEED = 2;
	/** [PNome] bracket expected. */
	public final static int E_M_BRACKET = 3;
	/** [PNome] empty string. */
	public final static int E_M_EMPTY   = 4;
	

	/** [Polynomial] empty polynomial string. */
	public final static int E_P_EMPTY_P = 11;
	/** [Polynomial] empty monomial list. */
	public final static int E_P_EMPTY   = 12;
	/** [Polynomial] the polynomial must not have positive and negative powers in the same time. */
	public final static int E_P_SIGN    = 13;

	private int code = 0;
	private String msg = "";

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 */
	public PolynomialException(int code, String msg)
	{
		super();
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception without detail message.
	 *
	 */
	public PolynomialException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public PolynomialException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public PolynomialException(Throwable cause)
	{
		this(0,"",cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case PolynomialException.E_M_PARSE_N:
				return "[PNome] number parse error";
			case PolynomialException.E_M_POWNEED:
				return "[PNome] '^' is expected after the variable";
			case PolynomialException.E_M_BRACKET:
				return "[PNome] bracket expected";
			case PolynomialException.E_M_EMPTY:
				return "[PNome] empty string";
			case PolynomialException.E_P_EMPTY_P:
				return "[Polynomial] empty polynomial string";
			case PolynomialException.E_P_EMPTY:
				return "[Polynomial] empty monomial list";
			case PolynomialException.E_P_SIGN:
				return "[Polynomial] the polynomial must not have positive and negative powers in the same time";
		}
		return "[Polynomial] unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error " + this.code + " : " + this.getMessageFromCode(this.code);
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (\"" + this.msg + "\").";

		return tmp;
	}

}


















