import Polynomial.PNome;
import Polynomial.PolynomialException;
import Polynomial.Polynomial;


public class tstPolynomial
{
	public static void main(String [] args)
	{
		Polynomial p = null;
		try
		{
			p = new Polynomial("+2.0*x^3-x^-1-3.0*x+7.0*x^(-1)+3*x^3 -2*x^7",'x');

			System.out.println(p);

			p.simplify();
	
			System.out.println(p);

			p.sort();

			System.out.println(p);

			System.out.print("\n");

			System.out.println(p.coefList() + " ... " + p.getMax());

			Polynomial e = new Polynomial("-x^-3 - x^-2",'x');
			e.sort();
			e.simplify();

			System.out.println("\n\n" + e.coefListFromZero());

		}
		catch(PolynomialException e)
		{
			System.out.println(p);
			e.printStackTrace();
		}
	}
}
